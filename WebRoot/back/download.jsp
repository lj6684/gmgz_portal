﻿<%@ page language="java" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
    <link rel="shortcut icon" href="./static/images/logo.ico" type="image/x-icon" />
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="description" content="吉林省女子职业教育指导中心" />
        <meta name="keywords" content="吉林省女子职业教育指导中心" />
        <title>吉林省女子职业教育指导中心</title>
        <link href="./static/css/jlwj.css" type="text/css" rel="stylesheet"/> 
        <script  type="text/javascript" src="./static/js/jquery.min.js"></script>
        <!--[if IE 6]>
        <script type="text/javascript" src="./static/js/png.js"></script>
        <script type="text/javascript">
          DD_belatedPNG.fix('img');
        </script>
        <![endif]-->
        <script type="text/javascript">
          $(function(){
            $(".nav ul li:has(ul)").hover(function(){
              $(this).children('ul').stop(true, true).slideDown(400);
            },function(){
              $(this).children('ul').stop(true, true).slideUp('fast')
            });
          }) 
        
         $(function(){
            $('.nav ul>li').hover(function(){
              $(this).addClass('active').siblings('.nav ul>li').removeClass('active');
            })
          })
        </script>
     </head>
    
    <body>
    	<jsp:include page="/topmenu.jsp" />
		<div class="main">
    
        <div class="leftcont pull-right">
          <jsp:include page="contact_box.jsp"/>
          <div class="clear"></div>
        </div> 

        <div class="rightcont pull-left">
          <div class="website">
            您现在的位置：<a href="index.jsp">首页</a>  > <a href="">下载专区 </a>
          </div>
          <div class="rightcont-cont">
            <h3>资源列表</h3>
            <div class="ny_news_list">
              <ul>
               <li><a href="./download/Shop.zip"><div class="download">点击下载&nbsp;<img src="./static/images/disk.png"/></div>店铺模板申请表</a></li>
			   <li><a href="./download/ProductPlatform.zip"><div class="download">点击下载&nbsp;<img src="./static/images/disk.png"/></div>产品对接平台入驻申请表</a></li>
               <li><a href="./download/Photoshop_CS5.zip"><div class="download">点击下载&nbsp;<img src="./static/images/disk.png"/></div>PhotoShop CS5</a></li>
               <li><a href="./download/qianniu.zip"><div class="download">点击下载&nbsp;<img src="./static/images/disk.png"/></div>千牛平台</a></li>
               <li><a href="./doc/1.doc"><div class="download">点击下载&nbsp;<img src="./static/images/word.png" width="16px" height="16px"/></div>第二届“吉林网姐”电子商务创业大赛实施方案</a></li>
               <li><a href="./doc/2.doc"><div class="download">点击下载&nbsp;<img src="./static/images/word.png" width="16px" height="16px"/></div>第二届“吉林网姐”电子商务创业大赛参赛选手报名表</a></li>
               <li><a href="./doc/3.doc"><div class="download">点击下载&nbsp;<img src="./static/images/word.png" width="16px" height="16px"/></div>第二届“吉林网姐”电子商务创业大赛参赛企业报名表</a></li>
               <li><a href="./doc/shenqingbiao.doc"><div class="download">点击下载&nbsp;<img src="./static/images/word.png"/ width="16px" height="16px"></div>吉林网姐电商创业园入驻申请表</a></li>
             </ul>
            </div>
          </div>
        </div>
        <div class="clear"></div>
        <div class="foot">
版权所有：吉林省女子职业教育指导中心 Copyright © 2014 吉ICP备14003352号 
<br/>地址：长春市宽城区北亚泰大街1360号   主办单位：吉林省女子职业教育指导中心   电话：0431-81175117 / 81175118 
        </div>
        <div align="center">
          <script type="text/javascript">document.write(unescape("%3Cspan id='_ideConac' %3E%3C/span%3E%3Cscript src='http://dcs.conac.cn/js/09/000/0000/60156293/CA090000000601562930003.js' type='text/javascript'%3E%3C/script%3E"));</script>
        </div>
      </div>   
    </body>
</html>
