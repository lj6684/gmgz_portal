﻿<%@ page language="java" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
    <link rel="shortcut icon" href="./static/images/logo.ico" type="image/x-icon" />
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="description" content="吉林省女子职业教育指导中心" />
        <meta name="keywords" content="吉林省女子职业教育指导中心" />
        <title>吉林省女子职业教育指导中心</title>
        <link href="./static/css/jlwj.css" type="text/css" rel="stylesheet"/> 
        <script  type="text/javascript" src="./static/js/jquery.min.js"></script>
        <!--[if IE 6]>
        <script type="text/javascript" src="./static/js/png.js"></script>
        <script type="text/javascript">
          DD_belatedPNG.fix('img');
        </script>
        <![endif]-->
        <script type="text/javascript">
          $(function(){
            $(".nav ul li:has(ul)").hover(function(){
              $(this).children('ul').stop(true, true).slideDown(400);
            },function(){
              $(this).children('ul').stop(true, true).slideUp('fast')
            });
          }) 
        
         $(function(){
            $('.nav ul>li').hover(function(){
              $(this).addClass('active').siblings('.nav ul>li').removeClass('active');
            })
          })
         $(function(){
          $('.websister_tab tr:even').css('background','#f4f4f4')
         })
        </script>
     </head>
    
    <body>
        <jsp:include page="/topmenu.jsp" />
		</div>
		<div class="main">
        <div class="leftcont pull-left">
          <jsp:include page="wjfc_left.jsp" />
          <jsp:include page="contact_box.jsp" />       
          <div class="clear"></div>
        </div> 

        <div class="rightcont pull-right">
          <div class="website">
            您现在的位置：<a href="index.jsp">首页</a>  > <a href="">网姐风采 </a>
          </div>
          <div class="rightcont-cont">
            <h3>创业大赛</h3>
            <div class="rightcont-text">
             <h4><p>“吉林网姐”电子商务创业技能实战大赛</p></h4>
              
              <p>为进一步提高女性自主创业能力和就业竞争能力，展现“吉林网姐”创业风采，省女子职业教育指导中心于2014年、2015年连续举办了两届“吉林网姐”电子商务创业技能大赛，帮助女性创业者实现“零投资、零库存、零风险”的创业体验，推动我省“吉林网姐”电子商务培训的跨越式发展，营造了良好的“大众创业 万众创新”的社会氛围，推动了“吉林网姐”电子商务创业项目的全面发展。</p>
              <img src="./static/images/cyds1.jpg" class="img">
              <img src="./static/images/cyds2.jpg" class="img">
              <p>大赛以“加强女性创业技能培训，展示‘吉林网姐’创业风采”为理念，由吉林省女子职业教育指导中心主办。参赛人员主要为“吉林网姐”电子商务培训的学员；各市(州)中小企业及从事电子商务创业的社会女性；从事电子商务创业的女大学生。大赛可以“个人形式”或“团队形式”参加。以团队形式参赛者，每队人数不得超过4人，其中须选定1名负责人，通过电子商务实战来锻炼和提高参赛团队或个人的电子商务创业技能，主要考核参赛人员淘宝网店基础建设能力、营销推广及电商实战运营能力。比赛将组织专业评委评审，确定其获奖等次，给予物质奖励及证书，凡报名参赛人员均有纪念奖。</p>
              <img src="./static/images/cyds3.jpg" class="img">
              <img src="./static/images/cyds4.jpg" class="img">
              <p>通过大赛，激励和引导全省广大女性投身电子商务领域创业就业，实现创业梦想。同时，对于推动我省经济发展、促进家庭和谐、维护社会稳定、实现妇女自身进步都起到了积极的作用。</p>
            </div>
          </div>
        </div>
        <div class="clear"></div>
        <div class="foot">
版权所有：吉林省女子职业教育指导中心 Copyright © 2014 吉ICP备14003352号 
<br/>地址：长春市宽城区北亚泰大街1360号   主办单位：吉林省女子职业教育指导中心   电话：0431-81175117 / 81175118 
        </div>
        <div align="center">
          <script type="text/javascript">document.write(unescape("%3Cspan id='_ideConac' %3E%3C/span%3E%3Cscript src='http://dcs.conac.cn/js/09/000/0000/60156293/CA090000000601562930003.js' type='text/javascript'%3E%3C/script%3E"));</script>
        </div>
      </div>   
    </body>
</html>
