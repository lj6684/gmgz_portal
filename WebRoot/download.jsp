<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page language="java" pageEncoding="UTF-8" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>吉林省长春市国民公证处</title>
    <meta name="keywords" content="吉林省长春市国民公证处">
    <meta name="description" content="吉林省长春市国民公证处">
    <link rel="stylesheet" type="text/css" href="content/main.css"/>
    <script src="scripts/main.js" type="text/javascript"></script>
    <!--[if IE 6]>
    <script src="scripts/iepng.js" type="text/javascript"></script>
    <script type="text/javascript">
        EvPNG.fix('div, ul, img, li, input ,a ,td');
    </script>
    <![endif]-->
</head>

<body>
<%@include file="_top.jsp" %>
<div class="box_main">
    <%@include file="_header.jsp" %>
    <div class="contact">
        <%@include file="_contactleft.jsp"%>
        <div class="cont_right">
            <div class="right_dv1">
                下载专区
            </div>
            <div class="right_dv2">
                <ul class="dv_list">
                    <li style="line-height: 40px;">
                        <img src="/images/word.png">&nbsp;<a href="/download/gangaotai.doc">港澳台公证申请表</a>
                    </li>
                    <li style="line-height: 40px;">
                        <img src="/images/word.png">&nbsp;<a href="/download/shewai.doc">涉外公证申请表</a>
                    </li>
                    <li style="line-height: 40px;">
                        <img src="/images/word.png">&nbsp;<a href="/download/minshi.doc">公证申请表(民事)</a>
                    </li>
                    <li style="line-height: 40px;">
                        <img src="/images/word.png">&nbsp;<a href="/download/jingji.doc">公证申请表(经济)</a>
                    </li>
                </ul>
            <div class="right_dv3"><!--底框--></div>
        </div>
    </div>
</div>
<%@include file="_youqing.jsp"%>
<%@include file="_ad.jsp"%>

<%@include file="_footer.jsp"%>
<!--背景阴影-->
</div>
</body>
</html>