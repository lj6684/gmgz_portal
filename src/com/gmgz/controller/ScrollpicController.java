package com.gmgz.controller;

import java.util.Date;

import com.jfinal.core.Controller;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.upload.UploadFile;
import com.gmgz.config.SystemConfig;
import com.gmgz.index.PicViewer;
import com.gmgz.model.Scrollpic;

public class ScrollpicController extends Controller {
	
	public void index() {
		int page = 1;
		Page<Scrollpic> pageRecord = Scrollpic.me.paginate(page, SystemConfig.getInstance().getPageSize(), "select *", "from scrollpic order by idx");
			
		setAttr("pageRecord", pageRecord);
		
		renderJsp("index.jsp");
	}
	
	public void form() {
		Integer id = getParaToInt("id");
		if(id != null && id > 0) {
			// 更新
			setAttr("scrollpic", Scrollpic.me.findById(id));
		}
		
		render("form.jsp");
	}
	
	public void save() {
		UploadFile file = getFile("img");
		//System.out.println(file.getSaveDirectory());
		
		Scrollpic scrollpic = getModel(Scrollpic.class, "scrollpic");
		if(file != null) {
			scrollpic.set("pic", file.getFileName());
		}
		scrollpic.set("createTime", new Date());
		
		if(scrollpic.get("id") == null) {
			// 新增
			scrollpic.save();
		} else if(scrollpic.getInt("id") > 0) {
			// 更新
			scrollpic.update();
		}
		
		// 刷新前端缓存
		PicViewer.refreshScrollpicCache();
		
		index();
	}
	
	public void delete() {
		Integer id = getParaToInt("id");
		if(id != null && id > 0) {
			boolean flag = Scrollpic.me.deleteById(id);
			if(flag) {
				// 刷新前端缓存
				PicViewer.refreshScrollpicCache();
				
				index();
			} else {
				renderText("删除图片失败");
			}
		} else {
			renderText("删除图片失败");
		}
	}

}
