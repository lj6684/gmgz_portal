﻿<%@ page language="java" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
    <link rel="shortcut icon" href="./static/images/logo.ico" type="image/x-icon" />
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="description" content="吉林省女子职业教育指导中心" />
        <meta name="keywords" content="吉林省女子职业教育指导中心" />
        <title>吉林省女子职业教育指导中心</title>
        <link href="./static/css/jlwj.css" type="text/css" rel="stylesheet"/> 
        <script  type="text/javascript" src="./static/js/jquery.min.js"></script>
        <!--[if IE 6]>
        <script type="text/javascript" src="./static/js/png.js"></script>
        <script type="text/javascript">
          DD_belatedPNG.fix('img');
        </script>
        <![endif]-->
        <script type="text/javascript">
          $(function(){
            $(".nav ul li:has(ul)").hover(function(){
              $(this).children('ul').stop(true, true).slideDown(400);
            },function(){
              $(this).children('ul').stop(true, true).slideUp('fast')
            });
          }) 
        
         $(function(){
            $('.nav ul>li').hover(function(){
              $(this).addClass('active').siblings('.nav ul>li').removeClass('active');
            })
          })
        </script>
     </head>
    
    <body>
        <jsp:include page="/topmenu.jsp" />
		<div class="main">
        <div class="leftcont pull-left">
          <jsp:include page="wjfc_left.jsp" />
          <jsp:include page="contact_box.jsp" />       
          <div class="clear"></div>
        </div> 

        <div class="rightcont pull-right">
          <div class="website">
            您现在的位置：<a href="index.jsp">首页</a>  > <a href="">网姐风采 </a>
          </div>
          <div class="rightcont-cont">
            <h3>网姐征文</h3>
            <div class="rightcont-text">
               <p>“吉林网姐”征文活动，采取了开放式的创作形式，不限题材，不限字数，讲述发生在我们身边的网姐创业故事，记录最真实的创业历程，抒发最朴素的创业情感，每一个鲜活的故事，表达的都是网姐创业的火热心声。希望“吉林网姐”自强不息的创业精神，能够激励和引导更多女性投身到电子商务创业的事业中。 </p>
               <div class="ny_news_list">
                  <ul>
                   <li><span>第19期学员 丁杰</span><a href="article01.html">结缘网姐成就美丽人生</a></li>
                   <li><span>第21期学员 王亭亭</span><a href="article02.html">寻梦之旅.</a></li>
                   <li><span>第16期学员 张玉洁</span><a href="article03.html">我和职教中心的故事</a></li>
                   <li><span>第18期学员 王丽</span><a href="article04.html">第一单的喜悦</a></li>
                   <li><span>第9期学员 刘慧茹</span><a href="article05.html">我创业我精彩</a></li>
                   <li><span>第21期学员 李雷</span><a href="article06.html">网店圆了我创业的梦</a></li>
                   <li><span>第17期学员 刘洁</span><a href="article07.html">自己的天空</a></li>
                   <li><span>第23期学员 吴圆</span><a href="article08.html">我们永远在一起</a></li>
                   <li><span>第6期学员 焦华娟</span><a href="article09.html">艰辛 泪水 喜悦----我的“淘宝”之路</a></li>
                   <li><span>第21期学员 黄越</span><a href="article10.html">“吉林网姐”培训有感</a></li>
                   <li><span>第1期学员 刘艳清</span><a href="article11.html">网销自有激情在 不用扬鞭自奋蹄</a></li>
                   <li><span>第28期学员 王春波</span><a href="article12.html">“吉林网姐” 创业者的一盏明灯</a></li>
                   <li><span>第21期学员 贾依依</span><a href="article13.html">坚持是一种态度</a></li>
                   <li><span>第20期学员 叶楠</span><a href="article14.html">创业路上 扬帆起航</a></li>

                   <li><span>第26期学员 刘玉红</span><a href="article15.html">感恩节的感恩心语</a></li>
                   <li><span>第28期学员 李昊阳</span><a href="article16.html">我的创业历程</a></li>
                   <li><span>第6期学员 叶丽萍</span><a href="article17.html">全职主妇的创业历程</a></li>
                   <li><span>第12期学员 王波</span><a href="article18.html">妈妈创业 不一样的体会</a></li>
                   <li><span>第10期学员 王艳艳</span><a href="article19.html">我的电商之路</a></li>
                   <li><span>第3期学员 刘万侠</span><a href="article20.html">分享一点经营心得</a></li>
                   <li><span>第14期学员 张婷</span><a href="article21.html">“淘宝”我的创业之路</a></li>

                   <li><span>第23期学员 姜晨雪</span><a href="article22.html">主妇创业 有艰辛 更有喜悦</a></li>
                   <li><span>第17期学员 母红梅</span><a href="article23.html">创业的源泉</a></li>
                   <li><span>第22期学员 高耀芳</span><a href="article24.html">服务是开启成功的一把钥匙</a></li>
                   <li><span>第11期学员 张春芳</span><a href="article25.html">暖  流</a></li>

                   <li><span>第14期学员 刘玉洁</span><a href="article26.html">四十岁的创业新兵</a></li>
                   <li><span>第2期学员 邵丽贤</span><a href="article27.html">为我的生活再添一笔色彩</a></li>
                   <li><span>第20期学员 马文华</span><a href="article28.html">用心经营 总有回报</a></li>
                   <li><span>第6期学员 王丽巍</span><a href="article29.html">展翅飞翔</a></li>
                 </ul>
              </div>
            </div>
          </div>
        </div>
        <div class="clear"></div>
        <div class="foot">
版权所有：吉林省女子职业教育指导中心 Copyright © 2014 吉ICP备14003352号 
<br/>地址：长春市宽城区北亚泰大街1360号   主办单位：吉林省女子职业教育指导中心   电话：0431-81175117 / 81175118 
        </div>
        <div align="center">
          <script type="text/javascript">document.write(unescape("%3Cspan id='_ideConac' %3E%3C/span%3E%3Cscript src='http://dcs.conac.cn/js/09/000/0000/60156293/CA090000000601562930003.js' type='text/javascript'%3E%3C/script%3E"));</script>
        </div>
      </div>   
    </body>
</html>
