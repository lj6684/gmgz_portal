package com.gmgz.interceptor;

import com.jfinal.aop.Interceptor;
import com.jfinal.core.ActionInvocation;
import com.jfinal.core.Controller;

public class AdminInterceptor implements Interceptor {

	public void intercept(ActionInvocation ai) {
		Controller controller = ai.getController();
		String adminName = controller.getSessionAttr("jlwj_admin");
		if(adminName == null) {
			// 无效管理员
			controller.redirect("/admin/index.jsp");
		} else {
			ai.invoke();
		}
	}

}
