﻿<%@ page language="java" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
    <link rel="shortcut icon" href="./static/images/logo.ico" type="image/x-icon" />
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="description" content="吉林省女子职业教育指导中心" />
        <meta name="keywords" content="吉林省女子职业教育指导中心" />
        <title>吉林省女子职业教育指导中心</title>
        <link href="./static/css/jlwj.css" type="text/css" rel="stylesheet"/> 
        <script  type="text/javascript" src="./static/js/jquery.min.js"></script>
        <!--[if IE 6]>
        <script type="text/javascript" src="./static/js/png.js"></script>
        <script type="text/javascript">
          DD_belatedPNG.fix('img');
        </script>
        <![endif]-->
      <script type="text/javascript">
          $(function(){
            $(".nav ul li:has(ul)").hover(function(){
              $(this).children('ul').stop(true, true).slideDown(400);
            },function(){
              $(this).children('ul').stop(true, true).slideUp('fast')
            });
          }) 
        
         $(function(){
            $('.nav ul>li').hover(function(){
              $(this).addClass('active').siblings('.nav ul>li').removeClass('active');
            })
          })
        </script>
     </head>
    
    <body>
    	<jsp:include page="/topmenu.jsp" />
		<div class="main">
         <div class="leftcont pull-left">
          <jsp:include page="zxgk_left.jsp" />
          <jsp:include page="contact_box.jsp" />
          <div class="clear"></div>
        </div> 

        <div class="rightcont pull-right">
           <div class="website">
            您现在的位置：<a href="index.jsp">首页</a>  > <a href="">走进中心 </a>
          </div>
          <div class="rightcont-cont">
            <h3>项目简介</h3>
            <div class="rightcont-text">
               <h4><p style="margin-bottom:10px;">“吉林网姐”电子商务培训项目简介</p></h4>
               <p>“吉林网姐”电子商务培训项目，是吉林省妇联推出的又一项妇女创业就业品牌公益项目。该项目为有电子商务创业意愿的女大学生或缺少创业技能和资金扶持的妇女提供免费培训课程及后续服务项目，帮助她们在创业初期实现“零投资、零库存、零风险”的创业保障，为女性朋友打造良好的创业就业平台。</p>
               <p>“吉林网姐”电子商务创业项目以“打造e平台，点燃她经济”为主旨，通过打造“技能培训、店企对接、e购服务、网店孵化、宣传展示”五位一体服务平台，启动“千姐千店千企（社）千品”计划（即：培训1000名“网姐”，孵化1000家网店，服务1000个社（企），推广1000种吉林特色产品），扶持创业初期女性实现“零投资、零风险”创业，帮助传统巾帼企业实现向电子商务企业转型。</p>
               <p>目前，中心拥有电子商务培训教室、电商创业园、女大学生创业园、“吉林网姐”与厂商产品对接室、“吉林网姐”摄影室、“吉林网姐”实训室等培训设施。中心组织专业人才编撰了“吉林网姐”电子商务培训教材，聘请有经验的专业教师团队授课。同时，中心还为学员免费提供网店产品拍照、店铺装修模板设计、实训指导及仓储等多项服务，使学员实现“零投资、零库存、零风险”创业，提升了她们的创业信心，解决了他们在创业初期的实际困难，降低了她们的创业门槛。</p>
               <!--  
               <p>2014年9月，中心在全省范围内组织开展了首届“吉林网姐”电子商务技能大赛，参与大赛的“吉林网姐”达到339人，经过四个月的比赛，涌现出了“六佳”和50强优秀代表，扩大了“吉林网姐”的品牌知名度，取得了良好的社会效益。</p>
               <p>截止2015年7月，已免费举办“吉林网姐”电子商务培训班72期，培训学员3693人，创办网店1084家。</p>
               -->
               <div style="padding-left:30px; padding-right:30px;">               
                 <div style="text-align:center"><h4>免费服务项目</h4></div>
                 <h4>模板设计</h4><div style="text-indent:30px;">免费为学员设计网店所需的店铺装修模板。根据学员的商品类别、风格特点设计实用美观的模板，并帮助学员完成店铺装修，使学员达到能够直接开店状态，累计制作店铺装修模板550个。</div>
                 <h4>产品拍摄</h4><div style="text-indent:30px;">为了方便学员进行产品拍照，中心建立了摄影室，并购置了摄影器材和相关设备，免费为不同行业、不同产品类别的网店提供服务，帮助学员拍摄美观清晰的商品图片，满足淘宝店主不同层次、不同方位的需求，累计为学员拍摄商品照片4万多张。</div>
                 <h4>产品对接</h4><div style="text-indent:30px;">定期举办“吉林网姐”产品对接会。为学员提供各类质优价廉产品，扶持“吉林网姐”成功开设网店，解决后顾之忧。通过产品对接会目前已有30余家企业、120余家个体、经销批发业户为学员提供千余种产品。</div>
                 <h4>实训课程</h4><div style="text-indent:30px;">为了进一步加强学员对电子商务相关操作技能的学习，帮助学员进行电子商务创业，中心专门设置了学员实训室，并安装了网络和电源等相关设施，免费对学员开放。通过指导教师针对图片处理，商品上传及店铺管理等实际操作的专业指导，进一步加强学员的职业技能，提升学员电子商务实战操作本领，增强学员的创业适应能力。</div>
                 <h4>电商创业园</h4><div style="text-indent:30px;">我们以已经开通淘宝店铺并实现运营的“吉林网姐”为服务对象，为入驻电商提供经营所需的场地和办公方面的共享设施，提供网店装修，产品拍摄、仓储物流、实践指导等方面的服务。安排专职人员指导学员开店，帮助学员解决开店中图片处理、产品上传、店铺管理等方面遇到的问题。</div>
               </div>
               <br/>
               <p>咨询电话：0431-81175117   81175118</p>
               <p>地址：长春市宽城区北亚泰大街与东天光路交汇</p>
               <p>乘车路线：可乘坐8路、11路、110路、274路公交车到天光路站下车即是</p>
            </div>
          </div>
        </div>
        <div class="clear"></div>
        <div class="foot">
版权所有：吉林省女子职业教育指导中心 Copyright © 2014 吉ICP备14003352号 
<br/>地址：长春市宽城区北亚泰大街1360号   主办单位：吉林省女子职业教育指导中心   电话：0431-81175117 / 81175118 
        </div>
        <div align="center">
          <script type="text/javascript">document.write(unescape("%3Cspan id='_ideConac' %3E%3C/span%3E%3Cscript src='http://dcs.conac.cn/js/09/000/0000/60156293/CA090000000601562930003.js' type='text/javascript'%3E%3C/script%3E"));</script>
        </div>
      </div>   
    </body>
</html>
