package com.gmgz.index;

import java.util.List;

import com.jfinal.plugin.activerecord.Page;
import com.gmgz.model.News;

public class NewsViewer {
	
	public static List<News> getNewsList(int pageIndex, int pageCount) {
		Page<News> pageRecord = News.me.paginate(pageIndex, pageCount, "select id, title, createTime", "from news order by createTime desc");
		return pageRecord.getList();
	}
	
	public static News getNews(int id) {
		News news = News.me.findById(id);
		
		int clicks = news.getInt("clicks");
		news.set("clicks", clicks + 1);
		news.update();
		
		return news;
	}

}
