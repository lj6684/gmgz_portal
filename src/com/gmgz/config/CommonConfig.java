package com.gmgz.config;

import com.gmgz.controller.*;
import com.gmgz.model.*;
import com.gmgz.util.Path;
import com.jfinal.config.*;
import com.jfinal.core.JFinal;
import com.jfinal.ext.interceptor.SessionInViewInterceptor;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.c3p0.C3p0Plugin;
import com.jfinal.render.JspRender;
import com.jfinal.render.ViewType;

public class CommonConfig extends JFinalConfig {

	@Override
	public void configConstant(Constants me) {
		loadPropertyFile("config.properties"); // 加载少量必要配置，随后可用getProperty(...)获取值
		me.setDevMode(getPropertyToBoolean("devMode", false));
		me.setViewType(ViewType.JSP); // 设置视图类型为Jsp，否则默认为FreeMarker

		// 相当回到1.8
		JspRender.setSupportActiveRecord(true);

		//me.setBaseViewPath("/admin");
		try {
			me.setUploadedFileSaveDirectory(Path.APP_PATH + "upload");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void configRoute(Routes me) {
        me.add("/", IndexController.class, "/index");	// 第三个参数为该Controller的视图存放路径

		// route for admin 
		me.add("/admin/news", NewsController.class);
		me.add("/admin/notice", NoticeController.class);
		me.add("/admin/login", LoginController.class);
		me.add("/admin/picnews", PicnewsController.class);
		me.add("/admin/scrollpic", ScrollpicController.class);
		me.add("/admin/catalog", CatalogController.class);
	}

	@Override
	public void configPlugin(Plugins me) {
		// 配置C3p0数据库连接池插件
		C3p0Plugin c3p0Plugin = new C3p0Plugin(getProperty("jdbcUrl"), getProperty("user"), getProperty("password").trim());
		me.add(c3p0Plugin);

		// 配置ActiveRecord插件
		ActiveRecordPlugin arp = new ActiveRecordPlugin(c3p0Plugin);
		me.add(arp);
		arp.addMapping("news", News.class); // 映射 news 表到 News 模型
		arp.addMapping("notice", Notice.class);
		arp.addMapping("admin", Admin.class);
		arp.addMapping("picnews", Picnews.class);
		arp.addMapping("scrollpic", Scrollpic.class);
		arp.addMapping("catalog", Catalog.class);
	}

	@Override
	public void configInterceptor(Interceptors me) {
		me.add(new SessionInViewInterceptor());

	}

	@Override
	public void configHandler(Handlers me) {
		// TODO Auto-generated method stub

	}
	
	@Override
	public void afterJFinalStart() {
	}

	@Override
	public void beforeJFinalStop() {

	}

	public static void main(String[] args) {
		JFinal.start("WebRoot", 8080, "/", 5);
	}

}
