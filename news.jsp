
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>机构要闻 - 成都公证处官方网站</title>
    <meta name="keywords" content="成都公证处官方网站">
    <meta name="description" content="成都公证处官方网站">
    <link rel="stylesheet" type="text/css" href="/skin/css/main.css"/>
    <script src="/skin/js/main.js" type="text/javascript"></script>
    <!--[if IE 6]>
    <script src="/skin/js/iepng.js" type="text/javascript"></script>
    <script type="text/javascript">
        EvPNG.fix('div, ul, img, li, input ,a ,td');
    </script>
    <![endif]-->
</head>

<body>
<div class="top">
    <div class="top_1">
        <div class="wecome">欢迎来到成都公证处官方网站！
            <script>
                var   CalendarData=new   Array(20);
                var   madd=new   Array(12);
                var   TheDate=new   Date();
                var   tgString="甲乙丙丁戊己庚辛壬癸";
                var   dzString="子丑寅卯辰巳午未申酉戌亥";
                var   numString="一二三四五六七八九十";
                var   monString="正二三四五六七八九十冬腊";
                var   weekString="日一二三四五六";
                var   sx="鼠牛虎兔龙蛇马羊猴鸡狗猪";
                var   cYear;
                var   cMonth;
                var   cDay;
                var   cHour;
                var   cDateString;
                var   DateString;
                var   Browser=navigator.appName;

                function   init()
                {
                    CalendarData[0]=0x41A95;
                    CalendarData[1]=0xD4A;
                    CalendarData[2]=0xDA5;
                    CalendarData[3]=0x20B55;
                    CalendarData[4]=0x56A;
                    CalendarData[5]=0x7155B;
                    CalendarData[6]=0x25D;
                    CalendarData[7]=0x92D;
                    CalendarData[8]=0x5192B;
                    CalendarData[9]=0xA95;
                    CalendarData[10]=0xB4A;
                    CalendarData[11]=0x416AA;
                    CalendarData[12]=0xAD5;
                    CalendarData[13]=0x90AB5;
                    CalendarData[14]=0x4BA;
                    CalendarData[15]=0xA5B;
                    CalendarData[16]=0x60A57;
                    CalendarData[17]=0x52B;
                    CalendarData[18]=0xA93;
                    CalendarData[19]=0x40E95;
                    madd[0]=0;
                    madd[1]=31;
                    madd[2]=59;
                    madd[3]=90;
                    madd[4]=120;
                    madd[5]=151;
                    madd[6]=181;
                    madd[7]=212;
                    madd[8]=243;
                    madd[9]=273;
                    madd[10]=304;
                    madd[11]=334;
                }

                function   GetBit(m,n)
                {
                    return   (m>>n)&1;
                }

                function   e2c()
                {
                    var   total,m,n,k;
                    var   isEnd=false;
                    var   tmp=TheDate.getYear();
                    if   (tmp<1900)     tmp+=1900;
                    total=(tmp-2001)*365
                            +Math.floor((tmp-2001)/4)
                            +madd[TheDate.getMonth()]
                            +TheDate.getDate()
                            -23;
                    if   (TheDate.getYear()%4==0&&TheDate.getMonth()>1)
                        total++;
                    for(m=0;;m++)
                    {
                        k=(CalendarData[m]<0xfff)?11:12;
                        for(n=k;n>=0;n--)
                        {
                            if(total<=29+GetBit(CalendarData[m],n))
                            {
                                isEnd=true;
                                break;
                            }
                            total=total-29-GetBit(CalendarData[m],n);
                        }
                        if(isEnd)break;
                    }
                    cYear=2001   +   m;
                    cMonth=k-n+1;
                    cDay=total;
                    if(k==12)
                    {
                        if(cMonth==Math.floor(CalendarData[m]/0x10000)+1)
                            cMonth=1-cMonth;
                        if(cMonth>Math.floor(CalendarData[m]/0x10000)+1)
                            cMonth--;
                    }
                    cHour=Math.floor((TheDate.getHours()+3)/2);
                }

                function   GetcDateString()
                {   var   tmp="";
                    tmp+=tgString.charAt((cYear-4)%10);       //年干
                    tmp+=dzString.charAt((cYear-4)%12);       //年支
                    tmp+="年(";
                    tmp+=sx.charAt((cYear-4)%12);
                    tmp+=")   ";
                    if(cMonth<1)
                    {
                        tmp+="闰";
                        tmp+=monString.charAt(-cMonth-1);
                    }
                    else
                        tmp+=monString.charAt(cMonth-1);
                    tmp+="月";
                    tmp+=(cDay<11)?"初":((cDay<20)?"十":((cDay<30)?"廿":"卅"));
                    if(cDay%10!=0||cDay==10)
                        tmp+=numString.charAt((cDay-1)%10);
                    tmp+="    ";
                    if(cHour==13)tmp+="夜";
                    tmp+=dzString.charAt((cHour-1)%12);
                    tmp+="时";
                    cDateString=tmp;
                    return   tmp;
                }

                function   GetDateString()
                {
                    var   tmp="";
                    var   t1=TheDate.getYear();
                    if   (t1<1900)t1+=1900;
                    tmp+=t1
                            +"年"
                            +(TheDate.getMonth()+1)+"月"
                            +TheDate.getDate()+"日   "
                            +TheDate.getHours()+":"
                            +((TheDate.getMinutes()<10)?"0":"")
                            +TheDate.getMinutes()
                            +"   星期"+weekString.charAt(TheDate.getDay());
                    DateString=tmp;
                    return   tmp;
                }

                init();
                e2c();
                GetDateString();
                GetcDateString();
                document.write("当前时间：",DateString);
            </script>

        </div>
        <div class="wecome_1">
            <table width="250" border="0" align="left" cellpadding="0" cellspacing="0">
                <tr>
                    <td><img src="/skin/images/img2.gif" width="13" height="12"/></td>
                    <td><a href="javascript:;" onclick="this.style.behavior='url(#default#homepage)';this.setHomePage(location.href);">设为首页</a></td>
                    <td><img src="/skin/images/img3.gif" width="15" height="12"/></td>
                    <td><a href="javascript:;" onclick="AddFavorite(window.location,document.title)">加入收藏</a></td>
                    <td><img src="/skin/images/img4.gif" width="15" height="14"/></td>
                    <td><a href="/list_137_1.html">联系我们</a></td>
                </tr>
            </table>

        </div>

    </div>
</div>
<div class="box_main">
    <!--背景阴影-->
    <div class="office">
        <div class="gong"><a href="/"><img src="/skin/images/img1.png" width="354" height="212"/></a></div>
        <object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,28,0" width="1000" height="260">
            <param name="movie" value="/banner.swf" />
            <param name="quality" value="high" />
            <param name="wmode" value="opaque">
            <param name="allowScriptAccess" value="sameDomain" />
            <embed src="/banner.swf" quality="high" wmode="transparent" pluginspage="http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash" type="application/x-shockwave-flash"  width="1000" height="260"></embed>
        </object>
        <!--<img src="/skin/images/img1.jpg" width="1000" height="260"/>-->
        <div class="gong_1">
            <form name="myform" method="get" action="/index.php">
                <input type="hidden" name="m" value="content">
                <input type="hidden" name="c" value="search">
                <input type="hidden" name="a" value="init">
                <input type="hidden" name="catid" value="37">
                <input type="hidden" name="dosubmit" value="1">
                <input type="hidden" name="info[catid]" value="0">
                <table width="320" border="0" align="center">
                    <tr>
                        <td>搜索：</td>
                        <td><input type="text" value="" name="info[title]" style=" width:250px; height:25px; border:none; background:url(/skin/images/img6.gif) center top no-repeat;"/></td>
                        <td><input type="submit" value="" name="" style="background:url(/skin/images/img5.gif) center no-repeat; width:25px; height:25px; border:none;"/></td>
                    </tr>
                </table>
            </form>
        </div>
    </div>

    <div class="daohang">
        <ul>
            <li><a href="/" >首&nbsp;&nbsp;页</a></li>
            <li ><a href="/list_36_1.html" >机构概况</a></li>
            <li ><a href="/list_38_1.html"  class="now">机构要闻</a></li>
            <li ><a href="/list_110_1.html" >党建之窗</a></li>
            <li ><a href="/list_126_1.html" >政策法规</a></li>
            <li ><a href="/list_132_1.html" >行业动态</a></li>
            <li ><a href="/list_109_1.html" >公证业务</a></li>
            <li ><a href="/list_129_1.html" >案例精选</a></li>
            <li ><a href="/list_134_1.html" >风采展示</a></li>
            <li style="background:none;"><a href="/list_147_1.html" >宣传视频</a></li>
        </ul>
    </div><div class="contact">
    <div class="cont_left">
        <div class="left_dv1">机构要闻</div>
        <div class="left_dv2">
            <ul>
                <li><a href="/list_38_1.html" class="not">机构要闻</a></li>
            </ul>
        </div>
        <div class="left_dv3"><!--底框--></div>
        <div class="left_dv1">联系我们</div>
        <div class="left_dv2">
            <div class="dv2"><img src="/skin/images/img36.gif" width="221" height="60"/></div>
            <div class="dv3"><span style="color:#999999; font-size:12px;">地址</span><br />成都市羊市街22号</div>
            <div class="dv4"><span style="color:#999999; font-size:12px; font-family:'新宋体';">E-MALL</span><br />CDGZC@QQ.COM</div>
            <div class="dv5"><img src="/skin/images/img39.gif" width="225" height="72" border="0" usemap="#Map3"/>
                <map name="Map3" id="Map3">
                    <area shape="rect" coords="75,43,164,69" href="http://weibo.com/cdgzc" />
                </map>
            </div>
        </div>
        <div class="left_dv3"><!--底框--></div>

    </div>    <div class="cont_right">
    <div class="right_dv1">
        <span style="float:right; font-size:14px; font-family:'新宋体'; font-weight:normal;">你现在的位置：<a href="/">首页</a> > <a  >机构要闻</a> > <a href="/list_38_1.html">机构要闻</a></span>机构要闻        </div>        <div class="right_dv2">
    <div class="dv_chuan">
        <a href="/show_38_1020_1.html" title="高龄老人在我处当场书写感谢信" ><img src="/uploadfile/2016/0422/thumb_140_118_20160422114336832.png" width="140" height="118"/></a>
        <div class="dv_bao">
            <h3 style="border-bottom:1px dotted #666;"><a href="/show_38_1020_1.html" title="高龄老人在我处当场书写感谢信"  style="" >高龄老人在我处当场书写感谢信</a></h3>
            　　4月20日下午，一位80多岁的老人来到我处申办公证事项。顺利办证后，老人在周敏副主任办公室对承办公证员颜丽表扬不停，还当场手书了一封表扬信。　　据了解，该老人当天在业务一部部长、公证员颜丽的热情接待下，顺利完成<a href="/show_38_1020_1.html" style="color:#e22b2a;">[详情]</a>
        </div>
    </div>
    <ul class="dv_list">
        <li><span style="float:right;">2016-04-22</span><a href="/show_38_1020_1.html" title="高龄老人在我处当场书写感谢信"  style="" >高龄老人在我处当场书写感谢信</a></li>
        <li><span style="float:right;">2016-04-19</span><a href="/show_38_1016_1.html" title="合肥市公证协会到我处考察交流"  style="" >合肥市公证协会到我处考察交流</a></li>
        <li><span style="float:right;">2016-04-19</span><a href="/show_38_1015_1.html" title="40位老人成为我处爱心联络员"  style="" >40位老人成为我处爱心联络员</a></li>
        <li><span style="float:right;">2016-04-15</span><a href="/show_38_1014_1.html" title="山东齐鲁公证处到我处交流考察"  style="" >山东齐鲁公证处到我处交流考察</a></li>
        <li><span style="float:right;">2016-04-08</span><a href="/show_38_1012_1.html" title="鲐背老人申办公证 我处公证员主动上门服务"  style="" >鲐背老人申办公证 我处公证员主动上门服务</a></li>
        <li><span style="float:right;">2016-04-08</span><a href="/show_38_1011_1.html" title="我市知识产权证据保全类公证专题研讨会顺利召开"  style="" >我市知识产权证据保全类公证专题研讨会顺利召开</a></li>
        <li><span style="float:right;">2016-03-30</span><a href="/show_38_1008_1.html" title="我处在全市公证工作会上受表彰"  style="" >我处在全市公证工作会上受表彰</a></li>
        <li><span style="float:right;">2016-03-30</span><a href="/show_38_1007_1.html" title="我处举行2016年第一季度业务考试"  style="" >我处举行2016年第一季度业务考试</a></li>
        <li><span style="float:right;">2016-03-29</span><a href="/show_38_1006_1.html" title="广州公证处到我处考察交流"  style="" >广州公证处到我处考察交流</a></li>
        <li><span style="float:right;">2016-03-24</span><a href="/show_38_1005_1.html" title="我处公证员夜赴医院上门办证"  style="" >我处公证员夜赴医院上门办证</a></li>
        <li><span style="float:right;">2016-03-15</span><a href="/show_38_997_1.html" title="我处开展3•15专题普法宣传活动"  style="" >我处开展3•15专题普法宣传活动</a></li>
        <li><span style="float:right;">2016-03-11</span><a href="/show_38_995_1.html" title="全国公证行业首个知识产权保护中心成立"  style="" >全国公证行业首个知识产权保护中心成立</a></li>
        <li><span style="float:right;">2016-03-08</span><a href="/show_38_994_1.html" title="成都公证处荣膺四川省三八红旗集体"  style="" >成都公证处荣膺四川省三八红旗集体</a></li>
        <li><span style="float:right;">2016-03-04</span><a href="/show_38_991_1.html" title="成都公证处可以轻松秒查房产信息啦！"  style="" >成都公证处可以轻松秒查房产信息啦！</a></li>
        <li><span style="float:right;">2016-03-01</span><a href="/show_38_989_1.html" title="我处远赴阿坝州黑水县开展爱心捐助活动"  style="" >我处远赴阿坝州黑水县开展爱心捐助活动</a></li>
        <li><span style="float:right;">2016-03-01</span><a href="/show_38_988_1.html" title="朋友圈证据保全公证火了  我处普法宣传忙不停"  style="" >朋友圈证据保全公证火了  我处普法宣传忙不停</a></li>
    </ul>
    <div class="dv_page"><a class="a1">338条</a> <a href="list_38_0.html" class="a1">上一页</a> <span style="color:red">1</span> <a href="list_38_2.html">2</a> <a href="list_38_3.html">3</a> <a href="list_38_4.html">4</a> <a href="list_38_5.html">5</a> <a href="list_38_6.html">6</a> <a href="list_38_7.html">7</a> <a href="list_38_8.html">8</a> <a href="list_38_9.html">9</a> <a href="list_38_10.html">10</a> ..<a href="list_38_22.html">22</a> <a href="list_38_2.html" class="a1">下一页</a></div>

</div>
    <div class="right_dv3"><!--底框--></div>
</div>
</div>
</div>
<div class="youqing">
    <div class="dvt_10"><!--友情链接---></div>
    <div class="dvt_11">

        <div class="vt_5" id="det">
            <div class="vt_6" id="indet">
                <div class="vt_7" id="det1">
                    <a href="http://www.scgzw.org/index.php" title="图片链接" target="_blank"><img src="/uploadfile/2014/0106/20140106034457747.gif" width="120" height="39"/></a>
                    <a href="http://www.scfz.gov.cn/" title="图片链接" target="_blank"><img src="/uploadfile/2014/0106/20140106034436764.gif" width="120" height="39"/></a>
                    <a href="http://www.moj.gov.cn/" title="图片链接" target="_blank"><img src="/uploadfile/2014/0106/20140106034415325.gif" width="120" height="39"/></a>
                    <a href="http://www.legalinfo.gov.cn/" title="百度" target="_blank"><img src="/uploadfile/2014/0106/20140106034353602.gif" width="120" height="39"/></a>
                    <a href="http://www.cdjustice.chengdu.gov.cn/cdssfj/" title="成都市司法局" target="_blank"><img src="/uploadfile/2014/0115/20140115023551311.jpg" width="120" height="39"/></a>
                    <a href="http://www.cdlegalaid.chengdu.gov.cn/cdflyz/" title="成都市法律援助" target="_blank"><img src="/uploadfile/2014/0115/20140115023658996.jpg" width="120" height="39"/></a>
                    <a href="http://www.zsgzc.com/" title="钟山公证处" target="_blank"><img src="/uploadfile/2014/0115/20140115023811610.jpg" width="120" height="39"/></a>
                </div>
                <div class="vt_8" id="det2"></div>
            </div>
        </div>
        <script type="text/javascript">ScrollLeft('det')</script>
    </div>

</div>   <!--
 <img src="/skin/images/img6.png" width="129" height="300" border="0" usemap="#Map" style="position:fixed; right:0px; top:260px; z-index:9999;"/>
<map name="Map" id="Map">
     <area shape="rect" coords="114,5,129,19" href="#" />
   </map>

-->
<div id="ShowAD" style="position:absolute;z-index:100;">

</div>
<script type="text/javascript">

    var bodyfrm = ( document.compatMode.toLowerCase()=="css1compat" ) ? document.documentElement : document.body;
    var adst = document.getElementById("ShowAD").style;
    adst.top = ( bodyfrm.clientHeight -530-22 ) + "px";
    adst.left = ( bodyfrm.clientWidth -155 ) + "px";
    function moveR() {
        adst.top = ( bodyfrm.scrollTop + bodyfrm.clientHeight - 530-22) + "px";
        adst.left = ( bodyfrm.scrollLeft + bodyfrm.clientWidth - 155 ) + "px";
    }
    setInterval("moveR();", 80);
    function closead()
    {
        adst.display='none';
    }


</script>

<img  src="/skin/images/img7.png" width="188" height="290" border="0" usemap="#Map2" style="position:fixed; left:0px; top:260px; z-index:9999;"/>
<map name="Map2" id="Map2">
    <area shape="rect" coords="83,113,173,140" href="http://weibo.com/cdgzc" />
</map>

<div class="foot" style=" position:relative;">
    <p style="TEXT-ALIGN: center">蜀ICP备12017934号 联系电话：028-86255556</p><p style="TEXT-ALIGN: center">地址：成都市羊市街22号 &nbsp;联系邮件：cdgzc@qq.com<br/>Copyright © 2011-2014 All Rights Reserved.</p>本网站总访问量 <font style="color:red">839666</font> ，当日访问量  <font style="color:red">654</font>
    <div style="position:absolute; right:150px; top:30px;">
        <script type="text/javascript">document.write(unescape("%3Cspan id='_ideConac' %3E%3C/span%3E%3Cscript src='http://dcs.conac.cn/js/23/333/0000/40370289/CA233330000403702890001.js' type='text/javascript'%3E%3C/script%3E"));</script>
    </div>
    &nbsp;&nbsp;技术支持：<a href="http://www.cdajcx.com" target="_blank">爱聚诚讯</a>
</div>
<!--背景阴影-->
</div>

<script language="JavaScript" src="/api.php?op=count&id=&modelid=1"></script>
</body>
</html>