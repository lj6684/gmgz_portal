﻿<%@ page language="java" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
    <link rel="shortcut icon" href="./static/images/logo.ico" type="image/x-icon" />
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="description" content="吉林省女子职业教育指导中心" />
        <meta name="keywords" content="吉林省女子职业教育指导中心" />
        <title>吉林省女子职业教育指导中心</title>
        <link href="./static/css/jlwj.css" type="text/css" rel="stylesheet"/> 
        <script  type="text/javascript" src="./static/js/jquery.min.js"></script>
        <!--[if IE 6]>
        <script type="text/javascript" src="./static/js/png.js"></script>
        <script type="text/javascript">
          DD_belatedPNG.fix('img');
        </script>
        <![endif]-->
        <script type="text/javascript">
          $(function(){
            $(".nav ul li:has(ul)").hover(function(){
              $(this).children('ul').stop(true, true).slideDown(400);
            },function(){
              $(this).children('ul').stop(true, true).slideUp('fast')
            });
          }) 
        
         $(function(){
            $('.nav ul>li').hover(function(){
              $(this).addClass('active').siblings('.nav ul>li').removeClass('active');
            })
          })
         $(function(){
          $('.websister_tab tr:even').css('background','#f4f4f4')
         })
        </script>
     </head>
    
    <body>
        <jsp:include page="/topmenu.jsp" />
		<div class="main">
        <div class="leftcont pull-left">
          <jsp:include page="wjfc_left.jsp" />
          <jsp:include page="contact_box.jsp" />       
          <div class="clear"></div>
        </div> 

        <div class="rightcont pull-right">
          <div class="website">
            您现在的位置：<a href="index.jsp">首页</a>  > <a href="">网姐风采 </a>
          </div>
          <div class="rightcont-cont">
            <h3>创业实践活动</h3>
            <div class="rightcont-text">
             <h4><p>“吉林网姐” 赴义乌学习考察</p></h4>
                            <p>为进一步推进“吉林网姐”电子商务培训项目顺利实施，提升“吉林网姐”创业的实战技能，2014年12月23日至26日，省女子职业教育指导中心组织并带领“吉林网姐”电子商务培训的学员一行18人赴浙江义乌学习考察。</p>
<p>针对学员关注的电子商务转型升级、店铺营销推广技巧、电商实战技能提升等问题，各位学员与老师展开了探讨，老师们以生动详实、深入浅出的文字、图像、表格分析等为学员解惑，并派遣义乌工商学院创业成功代表学生与各位“吉林网姐”进行现场座谈。其中，令人印象最深刻的是贾少华院长的讲座。作为义乌工商学院的创业教父，义乌电子商务教育界的引领者，他的讲谈更带给学员无限的激情与畅想。</p>

<p>在学习考察期间，“吉林网姐”实地踏查了真爱网商、康美义佳电商产业园、梦娜袜业等知名淘宝创业园区，通过实地踏查了解和座谈，“吉林网姐”们对网店的建设、运营和管理，对电子商务应用意识不断增强，应用技能得到有效提高。“吉林网姐”们表示，通过这4天的学习考察，坚定了经营网店的决心，激发了她们的创业热情，提升了她们电子商务创业的技巧，为更多的学员打开了电子商务创业的新思路。</p><br/>
<img src="./static/images/cysjhd_img.jpg" class="img">
<!--<img src="./static/images/cysjhd_img4.png" class="img">
<img src="./static/images/cysjhd_img5.png" class="img">
<img src="./static/images/cysjhd_img7.png" class="img">
-->
            </div>
          </div>
        </div>
        <div class="clear"></div>
        <div class="foot">
版权所有：吉林省女子职业教育指导中心 Copyright © 2014 吉ICP备14003352号 
<br/>地址：长春市宽城区北亚泰大街1360号   主办单位：吉林省女子职业教育指导中心   电话：0431-81175117 / 81175118 
        </div>
        <div align="center">
          <script type="text/javascript">document.write(unescape("%3Cspan id='_ideConac' %3E%3C/span%3E%3Cscript src='http://dcs.conac.cn/js/09/000/0000/60156293/CA090000000601562930003.js' type='text/javascript'%3E%3C/script%3E"));</script>
        </div>
      </div>   
    </body>
</html>
