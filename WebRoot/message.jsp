<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page language="java" pageEncoding="UTF-8" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>吉林省长春市国民公证处</title>
    <meta name="keywords" content="吉林省长春市国民公证处">
    <meta name="description" content="吉林省长春市国民公证处">
    <link rel="stylesheet" type="text/css" href="content/main.css"/>
    <script src="scripts/main.js" type="text/javascript"></script>
    <!--[if IE 6]>
    <script src="scripts/iepng.js" type="text/javascript"></script>
    <script type="text/javascript">
        EvPNG.fix('div, ul, img, li, input ,a ,td');
    </script>
    <![endif]-->
</head>

<body>
<%@include file="_top.jsp" %>
<div class="box_main">
    <%@include file="_header.jsp" %>
    <div class="contact">
        <%@include file="_contactleft.jsp"%>
        <div class="cont_right">
            <div class="right_dv1">
                在线留言
            </div>
            <div class="right_dv2">
                <div style="float:right;"><a href="#">查看更多留言...</a></div>
                <ul style="margin-top:25px;">
                    <li style="margin-top:15px;">
                        <div style="background-color: #B4D5FE; line-height: 28px; text-indent: 10px;">姓名: 丁丽敏</div>
                        <div style="text-indent: 10px;">留言: 您好，我想做房屋产权公证，你们那能做吗？你们是怎么收费的</div>
                        <div style="text-indent: 10px;">回复: 我公证处可以做涉及房产的公证事项，你表述的不清楚，你可以电话咨询或来我处咨询办理。</div>
                    </li>
                    <li style="margin-top:15px;">
                        <div style="background-color: #B4D5FE; line-height: 28px; text-indent: 10px;">姓名: 王世超</div>
                        <div style="text-indent: 10px;">留言: 想咨询出国留学 两证公证、办理时间和办理费用 ，翻译件是你们提供还是我自己翻译</div>
                        <div style="text-indent: 10px;">回复: 你提供的信息不完整，你可以电话咨询或来我处咨询办理。</div>
                    </li>
                    <li style="margin-top:15px;">
                        <div style="background-color: #B4D5FE; line-height: 28px; text-indent: 10px;">姓名: 刑丽丽</div>
                        <div style="text-indent: 10px;">留言: 我想咨询一下公证《资助出国留学协议书》的费用以及担保人需要提供的证件材料。</div>
                        <div style="text-indent: 10px;">回复: 出国留学人本人带身份证、《资助出国留学协议书》，担保人需本人带身份证、工作证、单位出具的收入证明。</div>
                    </li>
                </ul>
                <div style="margin-top:20px;color: #df8505">如您有任何问题，请用以下方法与我们联系，我们会尽快给您回复。</div>
                <table style="border: none">
                    <tr>
                        <td width="100px;">姓名</td>
                        <td width="300px;"><input type="text"><span style="color: red">*</span></td>
                    </tr>
                    <tr>
                        <td>电话</td>
                        <td><input type="text"><span style="color: red">*</span></td>
                    </tr>
                    <tr>
                        <td>E-Mail</td>
                        <td><input type="text"></td>
                    </tr>
                    <tr>
                        <td>留言</td>
                        <td><textarea rows="10" cols="30"></textarea><span style="color: red">*</span></td>
                    </tr>
                    <tr style="text-align: center">
                        <td colspan="2"><input type="button" value=" 提  交 "></td>
                    </tr>
                </table>
            <div class="right_dv3"><!--底框--></div>
        </div>
    </div>
</div>
<%@include file="_youqing.jsp"%>
<%@include file="_ad.jsp"%>

<%@include file="_footer.jsp"%>
<!--背景阴影-->
</div>
</body>
</html>