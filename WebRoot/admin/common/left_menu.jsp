<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<div class="sidebar-wrap">
  <div class="sidebar-content">
    <ul class="sidebar-list">
      <li><a href="#"><i class="icon-font">&#xe003;</i>发布信息管理</a>
        <ul class="sub-menu">
          <li><a href="<%=basePath %>admin/news/index?type=1"><i class="icon-font">&#xe008;</i>机构要闻</a></li>
          <li><a href="<%=basePath %>admin/news/index?type=2"><i class="icon-font">&#xe008;</i>政策法规</a></li>
          <li><a href="<%=basePath %>admin/news/index?type=3"><i class="icon-font">&#xe008;</i>公证动态</a></li>
          <li><a href="<%=basePath %>admin/news/index?type=4"><i class="icon-font">&#xe008;</i>公正业务</a></li>
          <li><a href="<%=basePath %>admin/picnews/index"><i class="icon-font">&#xe029;</i>图片新闻</a></li>
          <li><a href="<%=basePath %>admin/scrollpic/index"><i class="icon-font">&#xe044;</i>滚动图片</a></li>
          <li><a href="<%=basePath %>admin/notice/edit"><i class="icon-font">&#xe00a;</i>通知公告</a></li>
        </ul>
      </li>
    </ul>
  </div>
</div>