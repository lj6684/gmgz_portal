<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>

<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>门户后台管理系统</title>
<link rel="stylesheet" type="text/css" href="<%=basePath %>css/common.css" />
<link rel="stylesheet" type="text/css" href="<%=basePath %>css/main.css" />
<script type="text/javascript" src="<%=basePath %>js/libs/modernizr.min.js"></script>
</head>
<body>
	<div class="topbar-wrap white">
		<div class="topbar-inner clearfix">
			<div class="topbar-logo-wrap clearfix">
				<ul class="navbar-list clearfix">
					<li><a class="on" href="#">门户后台管理系统</a>
					</li>
				</ul>
			</div>
			<div class="top-info-wrap">
				<ul class="top-info-list clearfix">
					<li><a href="/admin/logout">退出</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="container clearfix">
        <!-- LEFT MENU -->
        <jsp:include page="../common/left_menu.jsp" />
		<!--/sidebar-->
		<div class="main-wrap">

			<div class="crumb-wrap">
				<div class="crumb-list">
					<i class="icon-font"></i><a href="index.html">首页</a><span
						class="crumb-step">&gt;</span><span class="crumb-name">滚动图片</span>
				</div>
			</div>
			<div class="result-wrap">
				<form name="myform" id="myform" method="post">
                    <div class="result-title">
                      <div class="result-list">
                        <c:if test="${pageRecord.list.size() < 8 }">
                          <a href="<%=basePath %>admin/scrollpic/form.jsp"><i class="icon-font"></i>添加图片</a>
                        </c:if>
                        &nbsp;&nbsp;<font color="gray">首页滚动图片数量最大支持8张</font>
                      </div>
                    </div>
                      
					<div class="result-content">
						<table class="result-tab" width="100%">
							<tr>
								<th width="5%">No.</th>
								<th>图片</th>
								<th>标题</th>
								<th>访问地址</th>
								<th>操作</th>
							</tr>
							<c:forEach items="${pageRecord.list }" var="scrollpic" varStatus="s">
							<tr>
								<td>${scrollpic.idx }</td>
                                <td><img src="<%=basePath %>upload/${scrollpic.pic }" width="50px;" height="50px;"/></td>
                                <td>${scrollpic.title }</td>
                                <td>${scrollpic.url }</td>
								<td><a class="link-update" href="form?id=${scrollpic.id }">修改</a> <a
									class="link-del" href="delete?id=${scrollpic.id }" onclick="return confirm('删除后无法恢复,确定要删除吗')">删除</a></td>
							</tr>
							</c:forEach>
						</table>
					</div>
				</form>
			</div>
		</div>
		<!--/main-->
	</div>
</body>
</html>