<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page language="java" pageEncoding="UTF-8" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>吉林省长春市国民公证处</title>
    <meta name="keywords" content="吉林省长春市国民公证处">
    <meta name="description" content="吉林省长春市国民公证处">
    <link rel="stylesheet" type="text/css" href="content/main.css"/>
    <script src="scripts/main.js" type="text/javascript"></script>
    <!--[if IE 6]>
    <script src="scripts/iepng.js" type="text/javascript"></script>
    <script type="text/javascript">
        EvPNG.fix('div, ul, img, li, input ,a ,td');
    </script>
    <![endif]-->
</head>

<body>
<%@include file="_top.jsp" %>
<div class="box_main">
    <%@include file="_header.jsp" %>
    <div class="contact">
        <%@include file="_contactleft.jsp"%>
        <div class="cont_right">
            <div class="right_dv1">
                <c:if test="${type == 1}">
                    机构要闻
                </c:if>
                <c:if test="${type == 2}">
                    政策法规
                </c:if>
                <c:if test="${type == 3}">
                    公证动态
                </c:if>
                <c:if test="${type == 4}">
                    公正业务
                </c:if>
            </div>
            <div class="right_dv2">
                <ul class="dv_list">
                    <c:forEach items="${pageRecord.list }" var="news">
                        <li style="line-height: 34px;">
                            <span style="float: right">
                                <fmt:formatDate value="${news.createTime }" pattern="yyyy-MM-dd" />
                            </span>
                            <div class="txt700">
                                <a href="view?id=${news.id }" title="${news.title }" style="">${news.title }</a>
                            </div>
                        </li>
                    </c:forEach>
                </ul>
                <div class="dv_page">
                    <!--
                    <span>共${pageRecord.totalRow }条</span>
                    -->
                    <span>
                        <c:if test="${pageRecord.totalPage > 10 }">
                            <c:set var="totalPageNumber" value="10"></c:set>
                        </c:if>
                        <c:if test="${pageRecord.totalPage <= 10}">
                            <c:set var="totalPageNumber" value="${pageRecord.totalPage }"></c:set>
                        </c:if>
                        <c:forEach var="item" varStatus="status" begin="1" end="${totalPageNumber }">
                            <a href="/list?page=${status.index }&type=${type }">${status.index }</a>
                        </c:forEach>
                    </span>
            </div>
            <div class="right_dv3"><!--底框--></div>
        </div>
    </div>
</div>
<%@include file="_youqing.jsp"%>
<%@include file="_ad.jsp"%>

<%@include file="_footer.jsp"%>
<!--背景阴影-->
</div>
</body>
</html>