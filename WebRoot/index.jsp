﻿<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>吉林省长春市国民公证处</title>
    <meta name="keywords" content="吉林省长春市国民公证处">
    <meta name="description" content="吉林省长春市国民公证处">
    <link rel="stylesheet" type="text/css" href="content/main.css"/>
    <script src="scripts/main.js" type="text/javascript"></script>
    <script src="scripts/jquery.min.js" type="text/javascript"></script>
    <script src="scripts/jquery.SuperSlide.js" type="text/javascript"></script>
    <!--[if IE 6]>
    <script src="scripts/iepng.js" type="text/javascript"></script>
    <script type="text/javascript">
        EvPNG.fix('div, ul, img, li, input ,a ,td');
    </script>
    <![endif]-->
</head>

<body>
<img src="/images/QRCode.jpg" style="position: fixed; right: 50px; bottom: 100px;" />
<%@include file="_top.jsp"%>
<div class="box_main">
<%@include file="_header.jsp"%>
<div class="jigou">
<div class="gou">
<!--机构要闻-->
<div class="dvt_1">
    <div class="ji">
        <span style="font-family:Aparajita; font-size:12px; font-weight:bold; float:right; line-height:32px;"><a
                href="/list?type=1">MORE+</a></span>机构要闻
    </div>
    <div class="ji_1">
        <div class="dv_1">
            <div id="slideBox" class="slideBox">
                <div class="hd">
                    <ul><li>1</li><li>2</li><li>3</li></ul>
                </div>
                <div class="bd">
                    <ul>
                        <li><a href="/view?id=203" target="_blank"><img src="/images/news1.jpg" /></a></li>
                        <li><a href="/view?id=204" target="_blank"><img src="/images/news2.jpg" /></a></li>
                        <li><a href="/view?id=205" target="_blank"><img src="/images/news3.jpg" /></a></li>
                    </ul>
                </div>
            </div>

            <script type="text/javascript">
                jQuery(".slideBox").slide({mainCell:".bd ul",autoPlay:true});
            </script>
        </div>
        <div class="dv_2">
            <!--
            <h3 style="text-align:center; line-height:30px; border-bottom:1px dotted #CCC;"><a
                    href="/show_38_1014_1.html" title="山东齐鲁公证处到我处交流考察" style="color:#666;"
                    style="">山东齐鲁公证处到我处交流考察...</a></h3>
                    -->
            <ul class="dv_3">
                <c:forEach items="${newsList }" var="news">
                    <li><span style="float:right;"><fmt:formatDate value="${news.createTime }" pattern="MM-dd" /></span>
                        <a href="/view?id=${news.id }" title="${news.title }" style=""><div class="txt300">${news.title }</div></a>
                    </li>
                </c:forEach>
            </ul>
        </div>

    </div>
</div>
<!--机构要闻-->

<div class="dvt_3">
    <object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"
            codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,28,0" width="714"
            height="68">
        <param name="movie" value="./flash/guanggao2.swf"/>
        <param name="quality" value="high"/>
        <param name="wmode" value="opaque">
        <param name="allowScriptAccess" value="sameDomain"/>
        <embed src="./flash/guanggao2.swf" quality="high" wmode="transparent"
               pluginspage="http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash"
               type="application/x-shockwave-flash" width="714" height="68"></embed>
    </object>
</div>

<!--党建之窗-->
<div class="dvt_2">
    <div class="dv_4">
        <div class="dang">
            <span style="font-family:Aparajita; font-size:12px; font-weight:bold; float:right; line-height:32px;">s<a
                    href="/list?type=2">MORE+</a></span>
            政策法规
        </div>
        <div class="dang_1">
            <ul>
                <c:forEach var="legislation" items="${legislationList }">
                    <li class="txt300"><a href="/view?id=${legislation.id }" title="${legislation.title }">${legislation.title }</a></li>
                </c:forEach>
            </ul>
        </div>
    </div>
    <div class="dv_5">
        <div class="dang">
            <span style="font-family:Aparajita; font-size:12px; font-weight:bold; float:right; line-height:32px;"><a
                    href="/list?type=3">MORE+</a></span>
            公证动态
        </div>
        <div class="dang_1">
            <ul>
                <c:forEach var="activity" items="${activityList }">
                    <li class="txt300"><a href="/view?id=${activity.id }" title="${activity.title }">${activity.title }</a></li>
                </c:forEach>
            </ul>
        </div>
    </div>
</div>
<!--党建之窗-->
<div class="dvt_3">
    <object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"
            codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,28,0" width="714"
            height="68">
        <param name="movie" value="./flash/guanggao1.swf"/>
        <param name="quality" value="high"/>
        <param name="wmode" value="opaque">
        <param name="allowScriptAccess" value="sameDomain"/>
        <embed src="./flash/guanggao1.swf" quality="high" wmode="transparent"
               pluginspage="http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash"
               type="application/x-shockwave-flash" width="714" height="68"></embed>
    </object>
</div>
<!--TAB--->
<div class="dvt_4">
    <!--
    <div class="dv_6" id="tab1">
        <ul>
            <li class="now"><a href="/list_132_1.html" style="color:#FFFFFF;">行业动态</a></li>
        </ul>
    </div>
    -->
    <div class="dv_9">
        <div class="v_10">
            公证业务
        </div> <span style="font-family:Aparajita; font-size:12px; font-weight:bold; float:right; margin-right: 5px; line-height:32px;"><a
            href="/list?type=4">MORE+</a></span>

        <div id="tablist1">
            <div class="tablist block">
                <ul class="dv_7">
                    <c:forEach var="business" items="${businessList }" begin="0" end="5">
                        <li class="txt300"><a href="/view?id=${business.id }" title="${business.title }">${business.title }</a></li>
                    </c:forEach>
                </ul>
                <ul class="dv_8">
                    <c:forEach var="business" items="${businessList }" begin="6" end="11">
                        <li class="txt300"><a href="/view?id=${business.id }" title="${business.title }">${business.title }</a></li>
                    </c:forEach>
                </ul>
            </div>
        </div>
    </div>
</div>


</div>

<div class="dvt_6">
    通知公告
</div>
<div class="dvt_7">
    长春市国民公证处办公时间：<br/>
    周一到周五，早8:30到11:30，下午1:30到4:30
</div>
<div class="dvt_8">交通指南</div>
<div class="dvt_9_1">
    长春市朝阳区清和街1555号，西中华路与清和街交汇。
</div>
<div class="dvt_8">网上服务</div>
<div class="dvt_9">
    <div class="v1"><a href="/view?id=200">公证流程</a></div>
    <div class="v2"><a href="/download.jsp">表格下载</a></div>
    <div class="v3"><a href="/message.jsp">在线留言</a> </div>
</div>
<%@include file="_gongzhengyuan.jsp"%>
</div>

</div>
<%@include file="_fengcai.jsp"%>
<%@include file="_youqing.jsp"%>
<%@include file="_ad.jsp"%>

<%@include file="_footer.jsp"%>
<!--背景阴影-->
</div>

<script language="JavaScript" src="scripts/api.js"></script>
</body>
</html>