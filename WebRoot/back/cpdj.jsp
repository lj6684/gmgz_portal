﻿<%@ page language="java" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
    <link rel="shortcut icon" href="./static/images/logo.ico" type="image/x-icon" />
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="description" content="吉林省女子职业教育指导中心" />
        <meta name="keywords" content="吉林省女子职业教育指导中心" />
        <title>吉林省女子职业教育指导中心</title>
        <link href="./static/css/jlwj.css" type="text/css" rel="stylesheet"/> 
        <script  type="text/javascript" src="./static/js/jquery.min.js"></script>
        <!--[if IE 6]>
        <script type="text/javascript" src="./static/js/png.js"></script>
        <script type="text/javascript">
          DD_belatedPNG.fix('img');
        </script>
        <![endif]-->
        <script type="text/javascript">
          $(function(){
            $(".nav ul li:has(ul)").hover(function(){
              $(this).children('ul').stop(true, true).slideDown(400);
            },function(){
              $(this).children('ul').stop(true, true).slideUp('fast')
            });
          }) 
        
         $(function(){
            $('.nav ul>li').hover(function(){
              $(this).addClass('active').siblings('.nav ul>li').removeClass('active');
            })
          })
        </script>
     </head>
    
    <body>
    	<jsp:include page="/topmenu.jsp" />
		<div class="main">

        <div class="leftcont pull-left">
          <jsp:include page="tsfw_left.jsp" />
          <jsp:include page="contact_box.jsp" />     
          <div class="clear"></div>
        </div> 

        <div class="rightcont pull-right">
           <div class="website">
            您现在的位置：<a href="index.jsp">首页</a>  > <a href="">特色服务 </a>
          </div>
          <div class="rightcont-cont">
            <h3>产品对接</h3>
            <div class="rightcont-text">
          <p>定期举办“吉林网姐”产品对接会，为学员提供各类质优价廉产品，扶持“吉林网姐”成功开设网店，解决后顾之忧。</p>
               <ul class="websister_li">
                <li>
                  <img alt="" src="./static/images/duijie/cpdj1.jpg"  class="img"/>
                  <span>产品对接图1</span>
                </li>
                <li>
                  <img alt="" src="./static/images/duijie/cpdj2.jpg"  class="img"/>
                  <span>产品对接图2</span>
                </li>
                <li>
                  <img alt="" src="./static/images/duijie/cpdj3.jpg"  class="img"/>
                  <span>产品对接图3</span>
                </li>
              <li>
                  <img alt="" src="./static/images/duijie/cpdj4.jpg"  class="img"/>
                  <span>产品对接图4</span>
                </li>
                <li>
                  <img alt="" src="./static/images/duijie/cpdj5.jpg"  class="img"/>
                  <span>产品对接图5</span>
                </li>
                  <li>
                  <img alt="" src="./static/images/duijie/cpdj6.jpg"  class="img"/>
                  <span>产品对接图6</span>
                </li>
                              </ul>
            </div>
          </div>
        </div>
        <div class="clear"></div>
        <div class="foot">
版权所有：吉林省女子职业教育指导中心 Copyright © 2014 吉ICP备14003352号 
<br/>地址：长春市宽城区北亚泰大街1360号   主办单位：吉林省女子职业教育指导中心   电话：0431-81175117 / 81175118 
        </div>
        <div align="center">
          <script type="text/javascript">document.write(unescape("%3Cspan id='_ideConac' %3E%3C/span%3E%3Cscript src='http://dcs.conac.cn/js/09/000/0000/60156293/CA090000000601562930003.js' type='text/javascript'%3E%3C/script%3E"));</script>
        </div>
      </div>   
    </body>
</html>
