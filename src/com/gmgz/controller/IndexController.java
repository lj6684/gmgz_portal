package com.gmgz.controller;

import com.gmgz.model.News;
import com.jfinal.core.ActionKey;
import com.jfinal.core.Controller;
import com.jfinal.plugin.activerecord.Page;

import java.util.List;

public class IndexController extends Controller {
	
	@ActionKey("/")
	public void index() {
        // 机构要闻
        List<News> newsList = News.me.find("select * from news where type=1 order by createTime desc limit 9");
        // 法律法规
        List<News> legislationList = News.me.find("select * from news where type=2 order by createTime desc limit 6");
        // 行业活动
        List<News> activityList = News.me.find("select * from news where type=3 order by createTime desc limit 6");
        // 公正业务
        List<News> businessList = News.me.find("select * from news where type=4 order by createTime desc limit 12");

        setAttr("newsList", newsList);
        setAttr("legislationList", legislationList);
        setAttr("activityList", activityList);
        setAttr("businessList", businessList);

		renderJsp("/index.jsp");
	}

    public void view() {
        int id = getParaToInt("id", 1);
        News news = News.me.findById(id);
        setAttr("news", news);
        renderJsp("/view.jsp");
    }

    public void list() {
        int page = getParaToInt("page", 1);
        int type = getParaToInt("type", 1);
        String order = getPara("order", "desc");
        Page<News> pageRecord = News.me.paginate(page, 15, "select *", "from news where type=? order by createTime " + order, type);

        setAttr("pageRecord", pageRecord);
        setAttr("type", type);

        renderJsp("/list.jsp");
    }

}
