<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page language="java" pageEncoding="UTF-8" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>吉林省长春市国民公证处</title>
    <meta name="keywords" content="吉林省长春市国民公证处">
    <meta name="description" content="吉林省长春市国民公证处">
    <link rel="stylesheet" type="text/css" href="content/main.css"/>
    <script src="content/main.js" type="text/javascript"></script>
    <!--[if IE 6]>
    <script src="scripts/iepng.js" type="text/javascript"></script>
    <script type="text/javascript">
        EvPNG.fix('div, ul, img, li, input ,a ,td');
    </script>
    <![endif]-->
</head>

<body>
<%@include file="_top.jsp" %>
<div class="box_main">
    <%@include file="_header.jsp" %>
    <div class="contact">
        <%@include file="_contactleft.jsp" %>
        <div class="cont_right">
            <div class="right_dv1">
                机构概况
            </div>
            <div class="right_dv2">
                <div class="dv_biao">
                    <h2>长春市国民公证处</h2>
                    <h2></h2>
                </div>
                <div class="dv_nei">
                    <p class="p_block">长春市国民公证处成立于1986年，是我市成立最早的公证处之一。目前，我处公证队伍已由最初的三名公证员发展壮大，现公证人员已达到二十余人，在长春地区属于规模较大的公证处之一。</p>
                    <p class="p_block">一直以来，我处的工作得到了上级机关和广大群众的充分肯定，多次在省、市级的各项评比中获得荣誉称号。市司法局、区政府、政协领导多次莅临我处检查指导工作，对我处各项工作给予了较高评价，在全省公证质量评比中，我处被评为“公证质量先进单位称号”。</p>
                    <p class="p_block">改革开放三十年来，我处服从服务于国家改革、开放、发展、稳定的大局，围绕全市经济和社会发展的中心工作，充分发挥公证工作服务、沟通、证明、监督的职能作用，实现了快速、良好发展，主要体现在：一是质优量大。1986年迄今，共办理涉外、涉港、澳、台和国内各类合同及民事公证事项几十万件，我处年办证数量也达近2万余件，圆满办结了多项全市重点建设项目；二是富有特色。近年来，我处办理了一系列新型公证，如企业经营责任制、股份公司创立、企业破产拍卖、政府采购、股票上市、股权转让、民间借贷的强制执行等；积极承办了诸多富有历史文化意义和社会公益性的公证业务。另外，还大力开展了对外贸易、劳务输出、机构设立、涉外诉讼、申请专利等涉外公证业务，促进了对外经济和服务贸易的规范化、合法化。特别是在近年来，我处响应省、市政府的号召，积极地投入到棚户区改造工作中去，为政府拆迁提供了优质的法律服务。</p>
                    <p class="p_block">我处在抓业务的同时还注重完善硬件和软件设备、改善办证条件、美化办公环境，实施公证利民、公证为民、公证便民工程，并坚决贯彻服务理念、不断提升服务品质，始终坚持质量第一、效率第一、服务社会的宗旨，着眼群众的根本利益，通过我处优质高效的公证法律服务，充分发挥公证在市场经济建设中的“服务、沟通、监督、证明”的职能作用，努力为建设和谐社会、为我市经济发展提供优质、高效的公证法律服务。</p>

                </div>

            </div>
            <div class="right_dv3"><!--底框--></div>

        </div>

    </div>


</div>
<%@include file="_youqing.jsp"%>
<%@include file="_ad.jsp"%>

<%@include file="_footer.jsp"%>
<!--背景阴影-->
</div>

</body>
</html>