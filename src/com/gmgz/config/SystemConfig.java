package com.gmgz.config;

import com.jfinal.kit.PropKit;

public class SystemConfig {
	
	public static SystemConfig instance;
	private int pageSize = 0;
	
	private SystemConfig() {
		PropKit.use("config.properties");
	}
	
	public static SystemConfig getInstance() {
		if(instance == null) {
			synchronized(SystemConfig.class) {
				if(instance == null) {
					instance = new SystemConfig();
				}
			}
		}
		return instance;
	}
	
	public int getPageSize() {
		if(pageSize == 0) {
			pageSize = PropKit.getInt("pageSize");
		}
		return pageSize;
	}

}
