﻿<%@ page language="java" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
    <link rel="shortcut icon" href="./static/images/logo.ico" type="image/x-icon" />
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="description" content="吉林省女子职业教育指导中心" />
        <meta name="keywords" content="吉林省女子职业教育指导中心" />
        <title>吉林省女子职业教育指导中心</title>
        <link href="./static/css/jlwj.css" type="text/css" rel="stylesheet"/> 
        <script  type="text/javascript" src="./static/js/jquery.min.js"></script>
        <!--[if IE 6]>
        <script type="text/javascript" src="./static/js/png.js"></script>
        <script type="text/javascript">
          DD_belatedPNG.fix('img');
        </script>
        <![endif]-->
       <script type="text/javascript">
        $(function(){
          $(".nav ul li:has(ul)").hover(function(){
            $(this).children('ul').stop(true, true).slideDown(400);
          },function(){
            $(this).children('ul').stop(true, true).slideUp('fast')
          });
        }) 
      </script>
     </head>
    
    <body>
    	<jsp:include page="/topmenu.jsp" />
		<div class="main">

        <div class="rightcont pull-left">
          <div class="website">
            您现在的位置：<a href="index.jsp">首页</a>  > <a href="">新闻中心 </a>
          </div>
          <div class="rightcont-cont">
            <h3>新闻内容</h3>
            <div class="rightcont-text">
             <h4><p>“吉林网姐”电子商务公益培训将于12月1日开班</p>
             <span>来源：吉林省女子职业教育指导中心   日期：2014-11-27</span>
             </h4>
               <p>第三十三期“吉林网姐”电子商务免费培训班将于2014年12月1日在吉林省女子职业教育指导中心举办，主要对电子商务现状、图片处理、店铺管理和推广等内容进行讲解。培训后为学员免费提供产品平台、仓储、物流等服务。无需个人投资，帮助学员实现“零投资、零库存、零风险”的创业体验。省内有志从事电子商务创业的女性及女大学生均可报名。</p><br/>
                    <p>同时，为了拓宽地方企业销售渠道，我们将免费为省内符合条件的企业创建网上店铺，并免费提供客服人员，详情来电咨询。</p><br/>

                <p>中心地址：长春市宽城区北亚泰大街与东天光路交汇</p><br/>

                <p>乘车路线：可乘坐8路、11路、110路、274路公交车到天光路站下车即是</p><br/>

                <p>联系人:韩老师  白老师</p><br/>

                <p>咨询电话：0431-81915529  81915528 </p><br/>

                <p>报名网站：http://www.jlwjcy.com</p>
            </div>
          </div>
        </div>
        <div class="clear"></div>
        <div class="foot">
版权所有：吉林省女子职业教育指导中心 Copyright © 2014 吉ICP备14003352号 
<br/>地址：长春市宽城区北亚泰大街1360号   主办单位：吉林省女子职业教育指导中心   电话：0431-81175117 / 81175118 
        </div>
        <div align="center">
          <script type="text/javascript">document.write(unescape("%3Cspan id='_ideConac' %3E%3C/span%3E%3Cscript src='http://dcs.conac.cn/js/09/000/0000/60156293/CA090000000601562930003.js' type='text/javascript'%3E%3C/script%3E"));</script>
        </div>
      </div>   
    </body>
</html>
