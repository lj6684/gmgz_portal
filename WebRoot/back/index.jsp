﻿<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%@page import="java.util.*" %>
<%@page import="com.gmgz.index.NewsViewer" %>
<%@page import="com.gmgz.index.NoticeViewer" %>
<%@page import="com.gmgz.model.News" %>
<%@page import="com.gmgz.model.Notice" %>

<%
  String path = request.getContextPath();
  String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <link rel="shortcut icon" href="./static/images/logo.ico" type="image/x-icon" />
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="description" content="长春市国民公证处" />
        <meta name="keywords" content="长春市国民公证处" />
        <title>长春市国民公证处</title>
        <link href="./static/css/jlwj.css" type="text/css" rel="stylesheet"/> 
        <script  type="text/javascript" src="./static/js/jquery.min.js"></script>
        <script type="text/javascript" src="./static/js/jquery.SuperSlide.js"></script>
        <script type=text/javascript src="./static/js/jquery.marquee.js"></script>
        <script type=text/javascript src="./static/js/vcode.js"></script>
        <!--[if IE 6]>
        <script type="text/javascript" src="./static/js/png.js"></script>
        <script type="text/javascript">
          DD_belatedPNG.fix('img');
        </script>
        <![endif]-->
        <script type="text/javascript">
          $(function(){
            $(".nav ul li:has(ul)").hover(function(){
              $(this).children('ul').stop(true, true).slideDown(400);
            },function(){
              $(this).children('ul').stop(true, true).slideUp('fast')
            });
            
            createCode();
          }) 
        
         $(function(){
            $('.nav ul>li').hover(function(){
              $(this).addClass('active').siblings('.nav ul>li').removeClass('active');
            })
          })
          
         function goRegist() {
         	//alert("test");
         	
         }
        </script>
        <script>
        var _hmt = _hmt || [];
        (function() {
          var hm = document.createElement("script");
          hm.src = "//hm.baidu.com/hm.js?3cbb8c11c2aa9bdd478f2083ac87b673";
          var s = document.getElementsByTagName("script")[0]; 
          s.parentNode.insertBefore(hm, s);
        })();
        </script>        
     </head>
    
    <body>
       <c:if test="${not empty alertMsg }">
        <script type="text/javascript">
        	alert("${alertMsg}");
        </script>
       </c:if>

        <jsp:include page="topmenu.jsp" />
		<div class="main">
        <jsp:useBean id="picnewsViewer" class="com.gmgz.index.PicViewer"></jsp:useBean>
        <c:set var="picnewsList" value="${picnewsViewer.getPicnewsList() }"></c:set>
        <c:set var="scrollpicList" value="${picnewsViewer.getScrollpicList() }"></c:set>
        <div class="newbox">
          <div class="news">
              <div class="focusBox" style="margin:0 auto">
                <ul class="pic">
                    <c:forEach items="${picnewsList }" var="picnews" varStatus="s">
                      <li><a href="${picnews.get('url') }"><img src="<%=basePath %>upload/${picnews.get('pic') }" width="320px;" height="247px;" /></a></li>
                    </c:forEach>
                </ul>
                <div class="txt-bg"></div>
                <div class="txt">
                  <ul>
                    <c:forEach items="${picnewsList }" var="picnews" varStatus="s">
                      <li><a href="${picnews.get('url') }">${picnews.get('title') }</a></li>
                    </c:forEach>
                  </ul>
                </div>
                <ul class="num">
                  <c:forEach items="${picnewsList }" var="picnews" varStatus="s">
                    <li><a>${s.index + 1 }</a></li>
                  </c:forEach>
                </ul>
            </div>

            <script type="text/javascript">
              jQuery(".focusBox").slide({ titCell:".num li", mainCell:".pic",effect:"fold", autoPlay:true,trigger:"click",
                //下面startFun代码用于控制文字上下切换
                startFun:function(i){
                   jQuery(".focusBox .txt li").eq(i).animate({"bottom":0}).siblings().animate({"bottom":-36});
                }
              });
            </script>
          </div>
          <div class="news_list">
            <h3><span><a href="list.jsp">&gt;&gt;更多</a></span>新闻动态</h3>
            <jsp:useBean id="newsViewer" class="com.gmgz.index.NewsViewer"></jsp:useBean>
            <ul>
              <c:forEach items="${newsViewer.getNewsList(1,7) }" var="news" varStatus="s">
                <li><span class="news_date"><fmt:formatDate value="${news.get('createTime') }" pattern="yyyy-MM-dd" /></span><a href="xw.jsp?id=${news.get('id') }"><div class="news_title_index">${news.get('title') }</div></a></li>
              </c:forEach>
             </ul>
          </div>

         <div class="notice">
           <!--  
           <h3><span><a href="#">更多</a></span>通知公告</h3>
           -->
           <h3>通知公告</h3>
           <div class="notice_cont">
              <div id="goup">
                <div id="goup1">
                 <jsp:useBean id="noticeViewer" class="com.gmgz.index.NoticeViewer"></jsp:useBean>
                 ${noticeViewer.getNotice().get("content") }
                </div><div id="goup2"></div></div> 
                <script type="text/javascript"> 
               	var speed=400; 
                  var FGgoup=document.getElementById('goup'); 
                  var FGgoup1=document.getElementById('goup1'); 
                  var FGgoup2=document.getElementById('goup2'); 
                  FGgoup2.innerHTML=FGgoup1.innerHTML 
                  function Marquee1(){ 
                  if(FGgoup2.offsetHeight-FGgoup.scrollTop<=0) 
                  FGgoup.scrollTop-=FGgoup1.offsetHeight 
                  else{ 
                  FGgoup.scrollTop++ 
                  } 
                  } 
                  var MyMar1=setInterval(Marquee1,speed) 
                  FGgoup.onmouseover=function() {clearInterval(MyMar1)} 
                  FGgoup.onmouseout=function() {MyMar1=setInterval(Marquee1,speed)} 
                </script> 
            </div>
          </div>
        </div> 
        <div class="banner">
          <a href="register"><img src="./static/images/banner2.jpg" width="1001px;" height="100px;"></a>
        </div>
        <div class="main_web">
          <div class="features_service">
            <h2> 业 务 介 绍</h2>
            <div class="service_module">
              <h3><span><a href="shoptemplate"><img src="./static/images/more-btn.jpg" /></a></span>店铺模板</h3>
              <a href="shoptemplate"><img src="./static/images/service_img01.jpg" class="service_module_img" /><p>为学员设计所需的店铺装修模板。根据学员的商品类别、风格特点设计实用美观的模板...</p></a>
            </div>

            <div class="service_module">
              <h3><span><a href="mfpz.html"><img src="./static/images/more-btn.jpg" /></a></span>商品拍照</h3>
              <a href="mfpz.html"><img src="./static/images/service_img02.jpg" class="service_module_img" /><p>为学员提供网店产品拍照等，为不同行业、不同产品类别的网店提供服务，满足学员不同方位的需求...</p></a>
            </div>
            <!--  
            <div class="service_module">
              <h3><span><a href="cpdj.html"><img src="./static/images/more-btn.jpg" /></a></span>产品对接</h3>
              <a href="cpdj.html"><img src="./static/images/service_img03.jpg" class="service_module_img" /><p>定期举办“吉林网姐”产品对接会，为学员提供各类质优价廉产品，扶持“吉林网姐”...</p></a>
            </div>
            -->
            <div class="service_module">
              <h3><span><a href="sxcz.html"><img src="./static/images/more-btn.jpg" /></a></span>实训操作</h3>
              <a href="sxcz.html"><img src="./static/images/service_img04.jpg" class="service_module_img" /><p>通过指导教师针对图片处理，商品上传及店铺管理等实际操作的专业指导，进一步提升学员的实操能力....</p></a>
            </div>

            <div class="service_module">
              <h3><span><a href="cyy.html"><img src="./static/images/more-btn.jpg" /></a></span>创业园</h3>
              <a href="cyy.html"><img src="./static/images/service_img05.jpg" class="service_module_img" /><p>以学员开办的网店为服务对象，为入驻电商提供经营所需的场地和办公方面的共享设施，提供仓储...</p></a>
            </div>

            <div class="service_module">
              <h3><span><a href="/company/index"><img src="./static/images/more-btn.jpg" /></a></span>对接平台</h3>
              <a href="/company/index"><img src="./static/images/service_img06.jpg" class="service_module_img" /><p>经营的品种包括特色农产品、母婴用品、服饰类、食品类、饰品类、家居类及延伸的系列产品... </p></a>
            </div>
          </div>
          <div class="right-cont">
            <div class="login">
               <h2>会员登录</h2>
                 <form id="loginForm" action="/login" onsubmit="checkForm();" method="post">
                    <table style="margin:8px 0 0 10px;">
                      <tr height=35>
                        <td width=50>用户名：</td>
                        <td colspan="2"><input type="text"  class="input_bg" name="nickName" /></td>
                      </tr>
                       <tr height=35>
                        <td>　密码：</td>
                        <td colspan="2"><input type="password" class="input_bg" name="password" /></td>
                      </tr>
                       <tr height=35>
                        <td>验证码：</td>
                        <td><input type="text" class="input_bg2" id="vcode"/></td>
                        <td><input type="text" onclick="createCode();" readonly="readonly" id="checkCode" class="unchanged" style="width: 60px" /></td>
                      </tr>
                       <tr height=30>
                        <td></td>
                        <td colspan="2"><input type="button" class="Loginbtn" onclick="submitForm();" value="登陆"></td>
                      </tr>
                    </table>
                 </form>
            </div>
            <a href="faq.jsp"><img src="./static/images/faq.jpg" style="border: 1px solid #FBC3D5;"/></a>
            <a href="contact.html"><img src="./static/images/banner01.jpg" alt="热线电话"></a>
          </div><!-- 登录注册 banner结束 -->
        </div>
        
        <div class="banner2">
          <a href="javascript:void(0);"><img src="./static/images/banner.jpg" width="999px;" height="100px;" style="border:2px solid #fcc1d5;"></a>
        </div>
        <div class="main_web">
        <div class="web_sister">
          <a href="pxzp.jsp" class="web_smore">更多</a>
          <div class="marquee level" direction="left" speed="30" step="1" pause="-1">
            <ul>
              <c:forEach items="${scrollpicList }" var="scrollpic" varStatus="s">
                <li style="text-align: center">
                <a href="${scrollpic.get('url') }">
                <img alt="${scrollpic.get('title') }" src="<%=basePath %>upload/${scrollpic.get('pic')}" width="156px;" height="104px;"/>
                <p>${scrollpic.get('title') }</p>
                </a>
              </li>
              </c:forEach>            
            </ul>
          </div>
        </div><!-- 吉林网姐结束 -->
        </div>
        <div class="foot">
版权所有：长春市国民公证处 Copyright © 2014 吉ICP备xxxx号
<br/>地址：长春市xxxx   主办单位：长春市国民公证处   电话：0431-xxx / xxx
        </div>
      </div>
    </body>
</html>
