<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>

<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>门户后台管理系统</title>
<link rel="stylesheet" type="text/css" href="<%=basePath %>css/common.css" />
<link rel="stylesheet" type="text/css" href="<%=basePath %>css/main.css" />

<link href="<%=basePath %>themes/default/css/umeditor.css" type="text/css" rel="stylesheet">
<script type="text/javascript" src="<%=basePath %>third-party/jquery.min.js"></script>
<script type="text/javascript" charset="utf-8" src="<%=basePath %>umeditor.config.js"></script>
<script type="text/javascript" charset="utf-8" src="<%=basePath %>umeditor.min.js"></script>
<script type="text/javascript" src="<%=basePath %>lang/zh-cn/zh-cn.js"></script>

<style type="text/css">
h1 {
	font-family: "微软雅黑";
	font-weight: normal;
}

.btn {
	display: inline-block;
	*display: inline;
	padding: 4px 12px;
	margin-bottom: 0;
	*margin-left: .3em;
	font-size: 14px;
	line-height: 20px;
	color: #333333;
	text-align: center;
	text-shadow: 0 1px 1px rgba(255, 255, 255, 0.75);
	vertical-align: middle;
	cursor: pointer;
	background-color: #f5f5f5;
	*background-color: #e6e6e6;
	background-image: -moz-linear-gradient(top, #ffffff, #e6e6e6);
	background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#ffffff),
		to(#e6e6e6) );
	background-image: -webkit-linear-gradient(top, #ffffff, #e6e6e6);
	background-image: -o-linear-gradient(top, #ffffff, #e6e6e6);
	background-image: linear-gradient(to bottom, #ffffff, #e6e6e6);
	background-repeat: repeat-x;
	border: 1px solid #cccccc;
	*border: 0;
	border-color: #e6e6e6 #e6e6e6 #bfbfbf;
	border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.25);
	border-bottom-color: #b3b3b3;
	-webkit-border-radius: 4px;
	-moz-border-radius: 4px;
	border-radius: 4px;
	filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffffffff',
		endColorstr='#ffe6e6e6', GradientType=0 );
	filter: progid:DXImageTransform.Microsoft.gradient(enabled=false );
	*zoom: 1;
	-webkit-box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.2), 0 1px 2px
		rgba(0, 0, 0, 0.05);
	-moz-box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.2), 0 1px 2px
		rgba(0, 0, 0, 0.05);
	box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.2), 0 1px 2px
		rgba(0, 0, 0, 0.05);
}

.btn:hover,.btn:focus,.btn:active,.btn.active,.btn.disabled,.btn[disabled]
	{
	color: #333333;
	background-color: #e6e6e6;
	*background-color: #d9d9d9;
}

.btn:active,.btn.active {
	background-color: #cccccc \9;
}

.btn:first-child {
	*margin-left: 0;
}

.btn:hover,.btn:focus {
	color: #333333;
	text-decoration: none;
	background-position: 0 -15px;
	-webkit-transition: background-position 0.1s linear;
	-moz-transition: background-position 0.1s linear;
	-o-transition: background-position 0.1s linear;
	transition: background-position 0.1s linear;
}

.btn:focus {
	outline: thin dotted #333;
	outline: 5px auto -webkit-focus-ring-color;
	outline-offset: -2px;
}

.btn.active,.btn:active {
	background-image: none;
	outline: 0;
	-webkit-box-shadow: inset 0 2px 4px rgba(0, 0, 0, 0.15), 0 1px 2px
		rgba(0, 0, 0, 0.05);
	-moz-box-shadow: inset 0 2px 4px rgba(0, 0, 0, 0.15), 0 1px 2px
		rgba(0, 0, 0, 0.05);
	box-shadow: inset 0 2px 4px rgba(0, 0, 0, 0.15), 0 1px 2px
		rgba(0, 0, 0, 0.05);
}

.btn.disabled,.btn[disabled] {
	cursor: default;
	background-image: none;
	opacity: 0.65;
	filter: alpha(opacity =       65);
	-webkit-box-shadow: none;
	-moz-box-shadow: none;
	box-shadow: none;
}
</style>

</head>
<body>
	<div class="topbar-wrap white">
		<div class="topbar-inner clearfix">
			<div class="topbar-logo-wrap clearfix">
				<ul class="navbar-list clearfix">
					<li><a class="on" href="#">门户后台管理系统</a></li>
				</ul>
			</div>
			<div class="top-info-wrap">
				<ul class="top-info-list clearfix">
					<li><a href="/admin/logout">退出</a></li>
				</ul>
			</div>
		</div>
	</div>
	<div class="container clearfix">
		<!-- LEFT MENU -->
        <jsp:include page="../common/left_menu.jsp" />
		<!--/sidebar-->
		<div class="main-wrap">
			<div class="crumb-wrap">
				<div class="crumb-list">
					<i class="icon-font"></i><a href="index.html">首页</a><span
						class="crumb-step">&gt;</span><span class="crumb-name">图片新闻</span>
				</div>
			</div>
			<div class="result-wrap">
				<form action="save" method="post" enctype="multipart/form-data">
					<input type="hidden" name="picnews.id" value="${picnews.id}">
					<table class="insert-tab" width="100%">
						<tbody>
							<tr>
								<th><i class="require-red">*</i>新闻图片：</th>
								<td>
                                  <input type="file" class="common-text required" name="img">
                                  <c:if test="${not empty picnews.pic }">
                                    &nbsp;<img src="<%=basePath %>upload/${picnews.pic }" width="50px;" height="50px;">
                                  </c:if>
                                  <br/><font color="gray">新闻图片尺寸 320px * 247px，图片名称不要包含中文</font>
                                </td>
							</tr>
							<tr>
								<th><i class="require-red">*</i>图片标题：</th>
								<td>
									<input class="common-text required" name="picnews.title" size="100" value="${picnews.title }" type="text">
								</td>
							</tr>
                            <tr>
                              <th><i class="require-red">*</i>新闻地址：</th>
                              <td>
                                <input class="common-text required" name="picnews.url" size="100" value="${picnews.url }" type="text">
                              </td>
                            </tr>
                            <tr>
                              <th><i class="require-red">*</i>显示顺序：</th>
                              <td>
                                <input class="common-text required" name="picnews.idx" value="${picnews.idx }" type="text">
                              </td>
                            </tr>
							<tr>
								<th></th>
								<td>
                                    <input class="btn btn-primary btn6 mr10" value="提交" type="submit"> 
                                    <input class="btn btn6" onclick="history.go(-1)" value="返回" type="button">
                                </td>
							</tr>
						</tbody>
					</table>

				</form>
			</div>
		</div>
		<!--/main-->
	</div>
</body>
</html>