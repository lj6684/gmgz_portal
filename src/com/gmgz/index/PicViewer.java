package com.gmgz.index;

import java.util.List;

import com.jfinal.plugin.activerecord.Page;
import com.gmgz.model.Picnews;
import com.gmgz.model.Scrollpic;

public class PicViewer {
	
	private static List<Picnews> picnewsList;
	private static List<Scrollpic> scrollpicList;
	
	public static void refreshPicnewsCache() {
		int page = 1;
		Page<Picnews> pageRecord = Picnews.me.paginate(page, 20, "select *", "from picnews order by idx");
		picnewsList = pageRecord.getList();
	}
	
	public static void refreshScrollpicCache() {
		int page = 1;
		Page<Scrollpic> pageRecord = Scrollpic.me.paginate(page, 20, "select *", "from scrollpic order by idx");
		scrollpicList = pageRecord.getList();
	}
	
	
	public static List<Picnews> getPicnewsList() {
		if(picnewsList == null) {
			refreshPicnewsCache();
		}
		return picnewsList;
	}
	
	public static List<Scrollpic> getScrollpicList() {
		if(scrollpicList == null) {
			refreshScrollpicCache();
		}
		return scrollpicList;
	}

}
