package com.gmgz.controller;

import com.jfinal.core.ActionKey;
import com.jfinal.core.Controller;
import com.jfinal.kit.EncryptionKit;
import com.gmgz.model.Admin;

public class LoginController extends Controller {
	
	@ActionKey("/admin/login")
	public void login() {
		String username = getPara("username");
		String pwd = getPara("password");
		
		String encPwd = EncryptionKit.sha1Encrypt(pwd); 
		
		Admin admin = Admin.me.findFirst("select * from admin where username=? and password=?", username, encPwd);
		if(admin != null) {
			setSessionAttr("jlwj_admin", admin.get("username"));
			
			forwardAction("/admin/news/index");
		} else {
			setAttr("notice", "<font color='red'>管理员认证失败</font>");
			renderJsp("/admin/index.jsp");
		}
	}
	
	@ActionKey("/admin/logout")
	public void logout() {
		removeSessionAttr("jlwj_admin");
		renderJsp("/admin/index.jsp");
	}
	
	
	
	public static void main(String[] args) {
		String res = EncryptionKit.sha1Encrypt("12345678");
		System.out.println(res);
	}

}
