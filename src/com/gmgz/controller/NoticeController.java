package com.gmgz.controller;

import com.jfinal.core.Controller;
import com.gmgz.index.NoticeViewer;
import com.gmgz.model.Notice;

public class NoticeController extends Controller {
	
	public void edit() {
		Integer id = 1;
		setAttr("notice", Notice.me.findById(id));
		
		render("form.jsp");
	}
	
	public void save() {
		Notice notice = getModel(Notice.class, "notice");
		notice.update();
		
		setAttr("message", "公告保存成功");
		setAttr("notice", notice);

		// 刷新前端缓存
		NoticeViewer.refreshCache();
				
		render("form.jsp");
	}

}
