﻿<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page import="com.gmgz.cache.*" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <link rel="shortcut icon" href="./static/images/logo.ico" type="image/x-icon" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="description" content="吉林省女子职业教育指导中心" />
<meta name="keywords" content="吉林省女子职业教育指导中心" />
<title>吉林省女子职业教育指导中心</title>
<link href="./static/css/jlwj.css" type="text/css" rel="stylesheet" />
<link href="./static/css/masonry.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="./static/css/lightbox.css" type="text/css" media="screen" />
<script type="text/javascript" src="./static/js/jquery.min.js"></script>
<script type="text/javascript">
        $(document).ready(function() {
          $(function(){
            $(".nav ul li:has(ul)").hover(function(){
              $(this).children('ul').stop(true, true).slideDown(400);
            },function(){
              $(this).children('ul').stop(true, true).slideUp('fast')
            });
          }) 
        
         $(function(){
            $('.nav ul>li').hover(function(){
              $(this).addClass('active').siblings('.nav ul>li').removeClass('active');
            })
          })
          
          $(".zoom,.ilike").hide();

          $(".zoom").each(function() { //遍历所有对象
            var src = $(this).siblings("img").attr("src");
            $(this).attr({
              href: src
            });
          });

          $("#nav li").click(function() {
            $("#nav a").removeClass("hover");
            $(this).find("a").addClass("hover");
          });

          $(".waterfall li").mouseover(function() {
            $(this).addClass("hover");
            $(this).find(".zoom,.ilike").show();
          });

          $(".waterfall li").mouseout(function() {
            $(this).removeClass("hover");
            $(this).find(".zoom,.ilike").hide();
          });
        });
        </script>
<!--[if IE 6]>
        <script type="text/javascript" src="./static/js/png.js"></script>
        <script type="text/javascript">
          DD_belatedPNG.fix('img');
        </script>
        <![endif]-->
</head>

<body>
  <script language="javascript">
        $(document).ready(function() {
          $(".tab_left a").each(function(j) {
            var num = j + 1;
            $(this).click(function() {
              $(".tab_left a").attr("class", "");
              $(this).attr("class", "hovera");
              for (var i = 1; i <= 7; i++) {
                $(".tab_kc" + i).css("display", "none");
              }
              $(".tab_kc" + num).css("display", "block");
              //每次选项时都触发瀑布流
              var $waterfall = $('.waterfall');
              $waterfall.masonry({
                columnWidth: 225
              });
              //每次选项时都触发瀑布流end

            });
          });
        });
  </script>
  <jsp:include page="/topmenu.jsp" />
	<div class="main">

    <div class="leftcont pull-left">
      <jsp:include page="tsfw_left.jsp" />
      <jsp:include page="contact_box.jsp" />      
      <div class="clear"></div>
    </div>
  
    <div class="rightcont pull-right">
      <div class="website">
        您现在的位置：<a href="index.jsp">首页</a> > <a href="">特色服务 </a>
      </div>
      <jsp:useBean id="shopTempList" class="com.gmgz.cache.ShopTempList"></jsp:useBean>
      <div class="rightcont-cont">
        <h3>店铺模板</h3>
        <div class="rightcont-text">
          <div class="position_pbl">
            <div id="tab">
              <div class="tab_left">
                <ul>
                  <c:forEach items="${shopTempList.getList() }" var="temp" varStatus="s">
                    <c:if test="${tid == temp.id }">
                      <!-- a class=hovera -->
                      <li class="active"><a href="shoptemplate?tid=${temp.id }" class="hovera">${temp.name }</a>
                      </li>
                    </c:if>
                    <c:if test="${tid != temp.id }">
                      <li><a href="shoptemplate?tid=${temp.id }">${temp.name }</a>
                      </li>
                    </c:if>
                  </c:forEach>
                </ul>
              </div>
            </div>

            <!-- main -->
            <div class="picbox  tab_kc1" style="display:block;">
              <ul class="waterfall">
                <c:forEach items="${fileNames }" var="fileName" varStatus="s" >
                  <li>
                    <div class="img_block">
                      <img width="220" height="300" src="./static/images/shoptemp/${folder }/${fileName }" /><a href="#" rel="lightbox[plants]" title="${fileName }" class="zoom">放大</a>
                      ${fileName.substring(0, fileName.lastIndexOf(".")) }
                    </div>
                  </li>
                </c:forEach>
              </ul>
            </div>
            <!-- main end -->
          </div>
          <script type="text/javascript" src="./static/js/jquery.masonry.min.js"></script>
          <script type="text/javascript" src="./static/js/lightbox.js"></script>
          <script type="text/javascript">
              $(document).ready(function() {
                var $waterfall = $('.waterfall');
                $waterfall.masonry({
                  columnWidth: 225
                });
              });
          </script>
        </div>
      </div>
    </div>
    <div class="clear"></div>
    <div class="foot">
      版权所有：吉林省女子职业教育指导中心 Copyright © 2014 吉ICP备14003352号 <br />地址：长春市宽城区北亚泰大街1360号 主办单位：吉林省女子职业教育指导中心 电话：0431-81175117 / 81175118
    </div>
    <div align="center">
      <script type="text/javascript">document.write(unescape("%3Cspan id='_ideConac' %3E%3C/span%3E%3Cscript src='http://dcs.conac.cn/js/09/000/0000/60156293/CA090000000601562930003.js' type='text/javascript'%3E%3C/script%3E"));</script>
    </div>
  </div>
</body>
</html>
