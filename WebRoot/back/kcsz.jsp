﻿<%@ page language="java" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
    <link rel="shortcut icon" href="./static/images/logo.ico" type="image/x-icon" />
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="description" content="吉林省女子职业教育指导中心" />
        <meta name="keywords" content="吉林省女子职业教育指导中心" />
        <title>吉林省女子职业教育指导中心</title>
        <link href="./static/css/jlwj.css" type="text/css" rel="stylesheet"/> 
        <script  type="text/javascript" src="./static/js/jquery.min.js"></script>
        <!--[if IE 6]>
        <script type="text/javascript" src="./static/js/png.js"></script>
        <script type="text/javascript">
          DD_belatedPNG.fix('img');
        </script>
        <![endif]-->
      <script type="text/javascript">
          $(function(){
            $(".nav ul li:has(ul)").hover(function(){
              $(this).children('ul').stop(true, true).slideDown(400);
            },function(){
              $(this).children('ul').stop(true, true).slideUp('fast')
            });
          }) 
        
         $(function(){
            $('.nav ul>li').hover(function(){
              $(this).addClass('active').siblings('.nav ul>li').removeClass('active');
            })
          })
        </script>
     </head>
    
    <body>
    	<jsp:include page="/topmenu.jsp" />
		<div class="main">
         <div class="leftcont pull-left">
          <jsp:include page="zxgk_left.jsp" />
          <jsp:include page="contact_box.jsp" />
          <div class="clear"></div>
        </div> 

        <div class="rightcont pull-right">
           <div class="website">
            您现在的位置：<a href="index.jsp">首页</a>  > <a href="">走进中心 </a>
          </div>
          <div class="rightcont-cont">
            <h3>课程设置</h3>
            <div class="rightcont-text">
             <h4><p>“吉林网姐”电子商务培训课程设置及培养目标</p></h4>
             <div align="center" style="padding-top:20px;">
             <table class="course_tab" border="0" cellpadding="0" cellspacing="0">
              <tr style="background-color: #dedede;">
                <td colspan="2">时间</td>
                <td width="130px;">课程名称</td>
                <td width="200px;">主要内容</td>
                <td>培养目标</td>
              </tr>
              <tr>
                <td rowspan="4">第一天</td>
                <td rowspan="2">上午</td>
                <td>电子商务概述</td>
                <td>电子商务现有模式，电子商务现状，电子商务发展趋势以及如何开展电子商务创业。</td>
                <td>让学员了解电子商务业务，掌握电子商务发展的现状和未来发展趋势。通过学习如何开展电子商务找到适合自己的创业平台。</td>
              </tr>
               <tr>
               <td>淘宝网发展历程 </td>
               <td>淘宝网的起源和发展，淘宝网发展历程分析，淘宝开店的优势。</td>
               <td>了解淘宝网的起源、发展及其历程，对淘宝创业有初步的认识。</td>
              </tr>
                <tr>
                <td rowspan="2">下午</td>
                <td>开店前期准备</td>
                <td>货源选择策略，热卖商品特点分析，商品定价策略，店铺定位分析。</td>
                <td>使学员掌握热卖商品的特点及货源选择的策略，能够根据自身情况和特点选择适合自己的货源。对店铺进行准确定位，具备开店的基本素质。</td>
              </tr>
               <tr>
               <td>淘宝网开店流程 </td>
               <td>淘宝账号注册，淘宝开店认证，支付宝认证。</td>
               <td>通过培训使学员能够掌握淘宝网开店的具体流程及申请店铺的基本技能，具备独立完成店铺申请认证的能力。</td>
              </tr>
              
                <tr>
                <td rowspan="4">第二天</td>
                <td rowspan="2">上午</td>
                <td>店铺基本设置</td>
                <td>填写店铺名称，店铺标志，店铺简介。</td>
                <td>使学员了解如何根据店铺经营商品的种类对店铺进行命名及制作店铺标志，能够根据店铺的基本情况编写店铺简介，完成店铺基本信息的设置。</td>
              </tr>
               <tr>
               <td>淘宝卖家规则 </td>
               <td>淘宝信用等级，商品发布规则，盗图行为的辨别及举证。</td>
               <td>通过培训使学员了解淘宝信用等级晋升制度，掌握商品发布的相关规则。通过实例分析，让学员了解哪些属于违规行为，掌握商品发布技巧。</td>
              </tr>
                <tr>
                <td rowspan="2">下午</td>
                <td>图片处理（一）</td>
                <td>图片处理软件基本操作。</td>
                <td>通过学习使学员对图片处理软件（Photoshop）有基本的了解，熟悉图片处理软件（Photoshop）的基本操作方法，掌握图片处理的基本操作（图像调整、填加水印、抠图等）。</td>
              </tr>
               <tr>
               <td>实践操作 </td>
               <td>实操训练。</td>
               <td>提升学员的动手操作能力。</td>
              </tr>
              
               <tr>
                <td rowspan="4">第三天</td>
                <td rowspan="2">上午</td>
                <td>内页文案策划</td>
                <td>内页文案常见问题分析，内页文案编写技巧，实例分析。</td>
                <td>通过实例讲解，使学员了解内页文案的作用及常见的问题，掌握内页文案编写技巧，提升学员内页文案编写能力。通过内页文案编写提高店铺商品销售转化率。</td>
              </tr>
               <tr>
               <td>商品发布 </td>
               <td>类目选择，商品基本信息、物流信息、售后保障信息及其他信息录入规则。</td>
               <td>使学员能够熟悉商品基本信息的填写方法和技巧，掌握商品发布流程，对售后保障信息能够正确选择，同时了解其它信息的录入规则和基本方法，达到独立发布商品的水平。</td>
              </tr>
                <tr>
                <td rowspan="2">下午</td>
                <td>图片处理（二）</td>
                <td>图片处理技巧。</td>
                <td>使学员能够独立应用图片处理软件对商品的图片进行处理，对常用的基本操作（图片的修复及美化、宝贝内页详情设计、动画制作等）加以熟悉，并能够简单应用。</td>
              </tr>
               <tr>
               <td>实践操作 </td>
               <td>实操训练。</td>
               <td>强化学员图片处理软件（Photoshop）的操作能力。</td>
              </tr>
              
                <tr>
                <td rowspan="4">第四天</td>
                <td rowspan="2">上午</td>
                <td>店铺装修（一）</td>
                <td>店铺整体布局规划，店铺基础页、详情页布局设计。</td>
                <td>根据所经营的商品类型特点，能够有针对性的对店铺进行特色布局管理，掌握店铺基础页、详情页的设计技巧，能够对店铺进行基础装修。</td>
              </tr>
               <tr>
               <td>物流操作流程 </td>
               <td>物流工具使用，物流服务介绍，物流数据分析。</td>
               <td>通过学习，了解淘宝网物流工具的使用方法、物流流程和物流服务，具备对淘宝网物流数据（发货、派送、退换货等）的分析能力，并能够通过数据分析进行优化，掌握物流模板设置方法，使其达到独立完成物流模板设置的目标。</td>
              </tr>
                <tr>
                <td rowspan="2">下午</td>
                <td>店铺装修（二）</td>
                <td>店铺装修模板制作及管理。</td>
                <td>掌握店铺装修的基本操作，对店铺装修的基本原则和基本要求有所了解，提高店铺装修模板制作及管理能力。根据经营商品的特点对店铺模板进行优化组合。</td>
              </tr>
               <tr>
               <td>网店的运营及管理</td>
               <td>商品管理，客户服务管理，日常运营管理。</td>
               <td>对所经营的商品能够有效的进行管理，熟悉商品的上下架、删除、橱窗推荐等操作；了解客户服务管理的基本流程和基本操作技巧，能够独立完成淘宝店铺的日常运营管理工作。</td>
              </tr>
              
                <tr>
                <td rowspan="2">第五天</td>
                <td >上午</td>
                <td>网店客服技巧</td>
                <td>网店客服应具备的基本素质、能力,相关知识，客服沟通技巧。</td>
                <td>掌握客服人员应具备的基本素质和能力，了解客服应具备的商品相关知识，熟练掌握客服沟通技巧。能够正确分析买家购物心理，做好售后服务及投诉处理工作，有效减少客户流失。</td>
              </tr>
                <tr>
                <td rowspan="2">下午</td>
                <td>产品对接会</td>
                <td>省内特色产品介绍。</td>
                <td>为学员提供产品平台。</td>
                </tr>
             </table>
             </div>
              
            </div>
          </div>
        </div>
        <div class="clear"></div>
        <div class="foot">
版权所有：吉林省女子职业教育指导中心 Copyright © 2014 吉ICP备14003352号 
<br/>地址：长春市宽城区北亚泰大街1360号   主办单位：吉林省女子职业教育指导中心   电话：0431-81175117 / 81175118 
        </div>
        <div align="center">
          <script type="text/javascript">document.write(unescape("%3Cspan id='_ideConac' %3E%3C/span%3E%3Cscript src='http://dcs.conac.cn/js/09/000/0000/60156293/CA090000000601562930003.js' type='text/javascript'%3E%3C/script%3E"));</script>
        </div>
      </div>   
    </body>
</html>
