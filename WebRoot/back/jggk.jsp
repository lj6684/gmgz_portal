﻿<%@ page language="java" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
    <link rel="shortcut icon" href="./static/images/logo.ico" type="image/x-icon" />
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="description" content="长春市国民公证处" />
        <meta name="keywords" content="长春市国民公证处" />
        <title>长春市国民公证处</title>
        <link href="./static/css/jlwj.css" type="text/css" rel="stylesheet"/> 
        <script  type="text/javascript" src="./static/js/jquery.min.js"></script>
        <!--[if IE 6]>
        <script type="text/javascript" src="./static/js/png.js"></script>
        <script type="text/javascript">
          DD_belatedPNG.fix('img');
        </script>
        <![endif]-->
         <script type="text/javascript">
          $(function(){
            $(".nav ul li:has(ul)").hover(function(){
              $(this).children('ul').stop(true, true).slideDown(400);
            },function(){
              $(this).children('ul').stop(true, true).slideUp('fast')
            });
          }) 
        
         $(function(){
            $('.nav ul>li').hover(function(){
              $(this).addClass('active').siblings('.nav ul>li').removeClass('active');
            })
          })
        </script>
     </head>
    
    <body>
    	<jsp:include page="/topmenu.jsp" />
		<div class="main">
        <div class="leftcont pull-left">
          <jsp:include page="zxgk_left.jsp" />
          <jsp:include page="contact_box.jsp" />
          <div class="clear"></div>
        </div> 

        <div class="rightcont pull-right">
          <div class="website">
            您现在的位置：<a href="index.jsp">首页</a>  > <a href="">走进中心 </a>
          </div>
          <div class="rightcont-cont">
            <h3>中心概况</h3>
            <div class="rightcont-text">
               <p>长春市国民公证处（以下简称中心），为吉林省妇联直属财政拨款公益性事业单位，2008年8月成立，位于亚泰北大街1360号，建筑面积1503.11平方米。</p>
    <p>中心职责是：指导和研究全省女子职业技术教育，进一步推动我省女子职业教育事业发展；负责全省女子职业    教育理论研究、信息咨询及全省女子职业教育信息平台建设工作。中心工作目标：以“春蕾职业技术教育助学”项目及“吉林网姐电子商务培训”项目为重点，资助贫困女生重返校园，掌握一技之长，同时，帮扶贫困女性及“春蕾女生”提高素质和就业技能，实现创业就业，增收致富。 </p>  
              </div>
          </div>
        </div>
        <div class="clear"></div>
        <div class="foot">
版权所有：长春市国民公证处 Copyright © 2014 吉ICP备14003352号 
<br/>地址：长春市宽城区北亚泰大街1360号   主办单位：长春市国民公证处   电话：0431-81175117 / 81175118 
        </div>
      </div>
    </body>
</html>
