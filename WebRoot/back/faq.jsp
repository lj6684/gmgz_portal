﻿<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%@page import="java.util.*" %>
<%@page import="com.gmgz.index.NewsViewer" %>
<%@page import="com.gmgz.model.News" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
    <link rel="shortcut icon" href="./static/images/logo.ico" type="image/x-icon" />
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="description" content="吉林省女子职业教育指导中心" />
        <meta name="keywords" content="吉林省女子职业教育指导中心" />
        <title>吉林省女子职业教育指导中心</title>
        <link href="./static/css/jlwj.css" type="text/css" rel="stylesheet"/> 
        <script  type="text/javascript" src="./static/js/jquery.min.js"></script>
        <!--[if IE 6]>
        <script type="text/javascript" src="./static/js/png.js"></script>
        <script type="text/javascript">
          DD_belatedPNG.fix('img');
        </script>
        <![endif]-->
        <script type="text/javascript">
          $(function(){
            $(".nav ul li:has(ul)").hover(function(){
              $(this).children('ul').stop(true, true).slideDown(400);
            },function(){
              $(this).children('ul').stop(true, true).slideUp('fast')
            });
          }) 
        
         $(function(){
            $('.nav ul>li').hover(function(){
              $(this).addClass('active').siblings('.nav ul>li').removeClass('active');
            })
          })
        </script>
     </head>
    
    <body>
        <jsp:include page="/topmenu.jsp" />
		<div class="main">
          <div class="website">
            您现在的位置：<a href="index.jsp">首页</a>  > <a href="">常见问题 </a>
          </div>
          <div class="leftcont pull-left">
            <jsp:include page="zxgk_left.jsp"/>
            <jsp:include page="contact_box.jsp"/>            
            <div class="clear"></div>
        </div> 
        <div class="rightcont pull-right">
          <div class="website">
            您现在的位置：<a href="index.jsp">首页</a>  > <a href="wjzw.html">常见问题 </a>
          </div>
          <div class="rightcont-cont">
            <h3>常见问题</h3>
            <div class="rightcont-text">
              <p><b>问：报名培训真的是全程免费吗？</b></p>
              <p>答：真真切切是免费，我们不收取任何材料费，培训费。</p>
              <hr style="width:80%; margin:10px; auto; border:1px dashed #fbc3d5;"/>
              <p><b>问：这边可以住嘛？</b></p>
              <p>答：食宿自理的哦。</p>
              <hr style="width:80%; margin:10px; auto; border:1px dashed #fbc3d5;"/>
              <p><b>问：报名培训真的是全程免费吗？</b></p>
              <p>答：真真切切是免费，我们不收取任何材料费，培训费。</p>
              <hr style="width:80%; margin:10px; auto; border:1px dashed #fbc3d5;"/>
              <p><b>问：需要带哪些证件资料？</b></p>
              <p>答：本人身份证原件、身份证复印件及二寸彩照一张。</p>
              <hr style="width:80%; margin:10px; auto; border:1px dashed #fbc3d5;"/>
              <p><b>问：你们一期培训是多少天？</b></p>
              <p>答：5天。</p>
              <hr style="width:80%; margin:10px; auto; border:1px dashed #fbc3d5;"/>
              <p><b>问：你们培训环境怎么样？</b></p>
              <p>答：人手一台电脑、还有宽敞明亮的教室。</p>
              <hr style="width:80%; margin:10px; auto; border:1px dashed #fbc3d5; "/>
              <p><b>问：你们培训地址具体在哪？</b></p>
              <p>答：北亚泰大街和天光路交汇处，旁边有钻石礼都小区。</p>
              <hr style="width:80%; margin:10px; auto; border:1px dashed #fbc3d5;"/>
              <p><b>问：你们一期培训学员人多吗？</b></p>
              <p>答：一期70人左右，经常爆满，所以报名从速哦。</p>
              <hr style="width:80%; margin:10px; auto; border:1px dashed #fbc3d5;"/>
              <p><b>问：怎么报名呢？</b></p>
              <p>答：登陆吉林省女子职业教育指导中心官方网站http://www.jlwjcy.com 首页即可报名</p>
              <p>或者在微信公众平台首页下方点击我要报名。</p>
              <hr style="width:80%; margin:10px; auto; border:1px dashed #fbc3d5;"/>
              <p><b>问：报名实训课的条件？</b></p>
              <p>答：参加实训课的学员须符合完成培训课程获得结业证书、已经成功开设淘宝网店这两个条件。</p>
              <hr style="width:80%; margin:10px; auto; border:1px dashed #fbc3d5;"/>
              <p><b>问：参加实训课需要自带电脑吗？</b></p>
              <p>答：不需要。中心实训室配备了网络和电脑等相关设施，免费对学员开放。</p>
              <hr style="width:80%; margin:10px; auto; border:1px dashed #fbc3d5;"/>
              <p><b>问：如何预约参加实训课？</b></p>
              <p>答：想预约的学员请在吉林网姐电子商务培训qq群中联系职教中心白雪老师预约。</p>
              <hr style="width:80%; margin:10px; auto; border:1px dashed #fbc3d5;"/>
              <p><b>问：实训课的培训时间安排是什么？</b></p>
              <p>答：实训课培训天数为2—4天；上课时间9：00—15:30</p>
              <hr style="width:80%; margin:10px; auto; border:1px dashed #fbc3d5;"/>
              <p><b>问：如何成为会员？</b></p>
              <p>答：成功在网站报名的学员，登陆官网首页，顶部导航选择[会员中心]栏目，点击左侧[会员激活]功能</p>
              <p>根据提示输入报名时所登记个人信息，即可激活会员身份</p>
              <hr style="width:80%; margin:10px; auto; border:1px dashed #fbc3d5;"/>
              <p><b>问：忘记会员密码？</b></p>
              <p>答：登陆官网首页，顶部导航选择[会员中心]栏目，点击左侧[会员激活]功能</p>
              <p>根据提示输入个人信息，即可查看或修改会员密码</p>
            </div>
          </div>
        </div>
        <div class="clear"></div>
        <div class="foot">
版权所有：吉林省女子职业教育指导中心 Copyright © 2014 吉ICP备14003352号 
<br/>地址：长春市宽城区北亚泰大街1360号   主办单位：吉林省女子职业教育指导中心   电话：0431-81175117 / 81175118 
        </div>
        <div align="center">
          <script type="text/javascript">document.write(unescape("%3Cspan id='_ideConac' %3E%3C/span%3E%3Cscript src='http://dcs.conac.cn/js/09/000/0000/60156293/CA090000000601562930003.js' type='text/javascript'%3E%3C/script%3E"));</script>
        </div>
      </div>   
    </body>
</html>
