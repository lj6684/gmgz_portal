<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>

<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>门户后台管理系统</title>
<link rel="stylesheet" type="text/css" href="<%=basePath %>css/common.css" />
<link rel="stylesheet" type="text/css" href="<%=basePath %>css/main.css" />
<script type="text/javascript" src="<%=basePath %>js/libs/modernizr.min.js"></script>
</head>
<body>
	<div class="topbar-wrap white">
		<div class="topbar-inner clearfix">
			<div class="topbar-logo-wrap clearfix">
				<ul class="navbar-list clearfix">
					<li><a class="on" href="#">门户后台管理系统</a>
					</li>
				</ul>
			</div>
			<div class="top-info-wrap">
				<ul class="top-info-list clearfix">
					<li><a href="/admin/logout">退出</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="container clearfix">
        <!-- LEFT MENU -->
        <jsp:include page="../common/left_menu.jsp" />
		<!--/sidebar-->
		<div class="main-wrap">

			<div class="crumb-wrap">
				<div class="crumb-list">
					<i class="icon-font"></i><a href="index.html">首页</a><span
						class="crumb-step">&gt;</span><span class="crumb-name">
                        <c:if test="${type == 1}">
                            机构要闻列表
                        </c:if>
                        <c:if test="${type == 2}">
                            政策法规列表
                        </c:if>
                        <c:if test="${type == 3}">
                            公证动态列表
                        </c:if>
                        <c:if test="${type == 4}">
                            公正业务列表
                        </c:if>
						</span>
				</div>
			</div>
			<div class="result-wrap">
				<form name="myform" id="myform" method="post">
					<div class="result-title">
						<div class="result-list">
							<a href="<%=basePath %>admin/news/create?type=${type}"><i class="icon-font"></i>
                                <c:if test="${type == 1}">
                                    +添加机构要闻
                                </c:if>
                                <c:if test="${type == 2}">
                                    +添加政策法规
                                </c:if>
                                <c:if test="${type == 3}">
                                    +添加公证动态
                                </c:if>
                                <c:if test="${type == 4}">
                                    +添加公正业务
                                </c:if>
                            </a>
						</div>
					</div>
					<div class="result-content">
						<table class="result-tab" width="100%">
							<tr>
								<th width="5%">No.</th>
								<th width="60%">标题</th>
								<th>创建时间</th>
								<th>浏览次数</th>
								<th>操作</th>
							</tr>
							<c:forEach items="${pageRecord.list }" var="news" varStatus="s">
							<tr>
								<td>${s.index + 1}</td>
								<td title="${news.title}">
									<a target="_blank" href="<%=basePath %>admin/news/view?id=${news.id }" title="${news.title }">${news.title }</a>
								</td>
								<td><fmt:formatDate value="${news.createTime }" pattern="yyyy-MM-dd" /></td>
								<td>${news.clicks}</td>
								<td><a class="link-update" href="<%=basePath %>admin/news/form?id=${news.id }">修改</a> <a
									class="link-del" href="<%=basePath %>admin/news/delete?id=${news.id }" onclick="return confirm('删除后无法恢复,确定要删除吗')">删除</a></td>
							</tr>
							</c:forEach>
						</table>
						<div class="list-page">
							<span> 共 ${pageRecord.totalRow } 条 ${pageRecord.pageNumber }/${pageRecord.totalPage } 页 </span>
							<span style="float:right"> 
								<c:if test="${pageRecord.totalPage > 10 }">
									<c:set var="totalPageNumber" value="10"></c:set>
								</c:if>
								<c:if test="${pageRecord.totalPage <= 10}">
									<c:set var="totalPageNumber" value="${pageRecord.totalPage }"></c:set>
								</c:if> 
								<c:forEach var="item" varStatus="status" begin="1" end="${totalPageNumber }">
									<a href="<%=basePath %>admin/news/index?page=${status.index }&type=${type }">${status.index }</a>
								</c:forEach>
							</span>
						</div>
					</div>
				</form>
			</div>
		</div>
		<!--/main-->
	</div>
</body>
</html>