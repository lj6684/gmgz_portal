package com.gmgz.index;

import com.gmgz.model.Notice;

public class NoticeViewer {
	
	private static Notice notice;
	
	public static void refreshCache() {
		notice = Notice.me.findById(1);
	}
	
	public static Notice getNotice() {
		if(notice == null) {
			refreshCache();
		}
		return notice;
	}

}
