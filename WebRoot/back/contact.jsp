﻿<%@ page language="java" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
    <link rel="shortcut icon" href="./static/images/logo.ico" type="image/x-icon" />
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="description" content="吉林省女子职业教育指导中心" />
        <meta name="keywords" content="吉林省女子职业教育指导中心" />
        <title>吉林省女子职业教育指导中心</title>
        <link href="./static/css/jlwj.css" type="text/css" rel="stylesheet"/> 
        <script  type="text/javascript" src="./static/js/jquery.min.js"></script>
        <!--[if IE 6]>
        <script type="text/javascript" src="./static/js/png.js"></script>
        <script type="text/javascript">
          DD_belatedPNG.fix('img');
        </script>
        <![endif]-->
        <script type="text/javascript">
          $(function(){
            $(".nav ul li:has(ul)").hover(function(){
              $(this).children('ul').stop(true, true).slideDown(400);
            },function(){
              $(this).children('ul').stop(true, true).slideUp('fast')
            });
          }) 
        
         $(function(){
            $('.nav ul>li').hover(function(){
              $(this).addClass('active').siblings('.nav ul>li').removeClass('active');
            })
          })
        </script>
     </head>
    
    <body>
        <jsp:include page="/topmenu.jsp" />
		<div class="main">

         <div class="leftcont pull-right">
          <jsp:include page="contact_box.jsp"/>
          <div class="clear"></div>
        </div> 

        <div class="rightcont pull-left">
          <div class="website">
            您现在的位置：<a href="index.jsp">首页</a>  > <a href="">新闻中心 </a>
          </div>
          <div class="rightcont-cont">
            <h3>新闻内容</h3>
            <div class="rightcont-text">
                <img src="./static/images/map.png" width="730px" height="700px"/>
                <div style="margin-left:20px; margin-top: 20px;">
                  <span>　　<b>中心地址：</b>吉林省长春市宽城区北亚泰大街1360号 </span><br/>
             	  <span>　　<b>乘车路线：</b>可乘坐8路、11路、110路、274路公共汽车到天光路站下车即是</span><br/> 
                  <span>　　<b>联系电话：</b>0431-81175117 / 81175118</span><br/>
                </div>            
              <!--  
              <script type="text/javascript" src="http://api.map.baidu.com/api?v=1.4&ak=您的密匙"></script>
              <div style="width:690px;height:400px;border:#ccc solid 1px;font-size:12px; margin-left:20px" id="map"></div>
                <script type="text/javascript">
                  //创建和初始化地图函数：
                  function initMap(){
                    createMap();//创建地图
                    setMapEvent();//设置地图事件
                    addMapControl();//向地图添加控件
                    addMapOverlay();//向地图添加覆盖物
                  }
                  function createMap(){ 
                    map = new BMap.Map("map"); 
                    map.centerAndZoom(new BMap.Point(125.337404,43.926111),15);
                  }
                  function setMapEvent(){
                    map.enableScrollWheelZoom();
                    map.enableKeyboard();
                    map.enableDragging();
                    map.enableDoubleClickZoom()
                  }
                  function addClickHandler(target,window){
                    target.addEventListener("click",function(){
                      target.openInfoWindow(window);
                    });
                  }
                  function addMapOverlay(){
                    var markers = [
                      {content:"我的备注",title:"吉林网姐职业教育中心",imageOffset: {width:0,height:3},position:{lat:43.922787,lng:125.343512}}
                    ];
                    for(var index = 0; index < markers.length; index++ ){
                      var point = new BMap.Point(markers[index].position.lng,markers[index].position.lat);
                      var marker = new BMap.Marker(point,{icon:new BMap.Icon("http://api.map.baidu.com/lbsapi/createmap/images/icon.png",new BMap.Size(20,25),{
                        imageOffset: new BMap.Size(markers[index].imageOffset.width,markers[index].imageOffset.height)
                      })});
                      var label = new BMap.Label(markers[index].title,{offset: new BMap.Size(25,5)});
                      var opts = {
                        width: 200,
                        title: markers[index].title,
                        enableMessage: false
                      };
                      var infoWindow = new BMap.InfoWindow(markers[index].content,opts);
                      marker.setLabel(label);
                      addClickHandler(marker,infoWindow);
                      map.addOverlay(marker);
                    };
                  }
                  //向地图添加控件
                  function addMapControl(){
                    var scaleControl = new BMap.ScaleControl({anchor:BMAP_ANCHOR_BOTTOM_LEFT});
                    scaleControl.setUnit(BMAP_UNIT_IMPERIAL);
                    map.addControl(scaleControl);
                    var navControl = new BMap.NavigationControl({anchor:BMAP_ANCHOR_TOP_LEFT,type:BMAP_NAVIGATION_CONTROL_LARGE});
                    map.addControl(navControl);
                    var overviewControl = new BMap.OverviewMapControl({anchor:BMAP_ANCHOR_BOTTOM_RIGHT,isOpen:true});
                    map.addControl(overviewControl);
                  }
                  var map;
                    initMap();
                </script>
              <span>　　<b>中心地址：</b>吉林省长春市宽城区北亚泰大街1360号 </span><br/>
              <span>　　<b>乘车路线：</b>可乘坐8路、11路、110路、274路公共汽车到天光路站下车即是</span><br/> 
              <span>　　<b>联系电话：</b>0431-81915529 / 81915528</span><br/>
            </div>
          </div>
          -->
             
          </div>
        </div>
        </div>
        <div class="clear"></div>
        <div class="foot">
版权所有：吉林省女子职业教育指导中心 Copyright © 2014 吉ICP备14003352号 
<br/>地址：长春市宽城区北亚泰大街1360号   主办单位：吉林省女子职业教育指导中心   电话：0431-81175117 / 81175118 
        </div>
        <div align="center">
          <script type="text/javascript">document.write(unescape("%3Cspan id='_ideConac' %3E%3C/span%3E%3Cscript src='http://dcs.conac.cn/js/09/000/0000/60156293/CA090000000601562930003.js' type='text/javascript'%3E%3C/script%3E"));</script>
        </div>
      </div>   
    </body>
</html>
