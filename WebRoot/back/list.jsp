﻿<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%@page import="java.util.*"%>
<%@page import="com.gmgz.index.NewsViewer"%>
<%@page import="com.gmgz.model.News"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <link rel="shortcut icon" href="./static/images/logo.ico" type="image/x-icon" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="description" content="吉林省女子职业教育指导中心" />
<meta name="keywords" content="吉林省女子职业教育指导中心" />
<title>吉林省女子职业教育指导中心</title>
<link href="./static/css/jlwj.css" type="text/css" rel="stylesheet" />
<script type="text/javascript" src="./static/js/jquery.min.js"></script>
<!--[if IE 6]>
        <script type="text/javascript" src="./static/js/png.js"></script>
        <script type="text/javascript">
          DD_belatedPNG.fix('img');
        </script>
        <![endif]-->
<script type="text/javascript">
          $(function(){
            $(".nav ul li:has(ul)").hover(function(){
              $(this).children('ul').stop(true, true).slideDown(400);
            },function(){
              $(this).children('ul').stop(true, true).slideUp('fast')
            });
          }) 
        
         $(function(){
            $('.nav ul>li').hover(function(){
              $(this).addClass('active').siblings('.nav ul>li').removeClass('active');
            })
          })
        </script>
</head>

<body>
    <jsp:include page="/topmenu.jsp" />
  <div class="main">
    <div class="leftcont pull-right">
      <jsp:include page="contact_box.jsp" />
      <div class="clear"></div>
    </div>

    <div class="rightcont pull-left">
      <div class="website">
        您现在的位置：<a href="index.jsp">首页</a> > <a href="">新闻列表 </a>
      </div>
      <div class="rightcont-cont">
        <h3>新闻列表</h3>
        <div class="ny_news_list">
          <%
            	int pageIndex = 1;
            	String pageStr = request.getParameter("page");
            	if(pageStr != null && !pageStr.equals("")) {
            		pageIndex = Integer.parseInt(pageStr);
            	}
                request.setAttribute("pageIndex", pageIndex);
            %>
          <jsp:useBean id="newsViewer" class="com.gmgz.index.NewsViewer"></jsp:useBean>
          <ul>
            <c:forEach items="${newsViewer.getNewsList(pageIndex,15) }" var="news" varStatus="s">
              <li>
                <span class="news_date"><fmt:formatDate value="${news.get('createTime') }" pattern="yyyy-MM-dd" /></span>
                <a href="xw.jsp?id=${news.get('id') }"><div class="news_title_list">${news.get("title") }</div></a>
              </li>
            </c:forEach>
          </ul>
          <div class="page">
            <c:forEach begin="1" end="5" varStatus="s">
              <a href="list.jsp?page=${s.index }">${s.index }</a>
            </c:forEach>
          </div>
        </div>
      </div>
    </div>
    <div class="clear"></div>
    <div class="foot">
      版权所有：吉林省女子职业教育指导中心 Copyright © 2014 吉ICP备14003352号 <br />地址：长春市宽城区北亚泰大街1360号 主办单位：吉林省女子职业教育指导中心 电话：0431-81175117 / 81175118
    </div>
    <div align="center">
          <script type="text/javascript">document.write(unescape("%3Cspan id='_ideConac' %3E%3C/span%3E%3Cscript src='http://dcs.conac.cn/js/09/000/0000/60156293/CA090000000601562930003.js' type='text/javascript'%3E%3C/script%3E"));</script>
        </div>
  </div>
</body>
</html>
