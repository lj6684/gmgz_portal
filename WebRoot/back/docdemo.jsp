﻿<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%@page import="java.util.*"%>
<%
String ser = "./static/flexpaper3/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <link rel="shortcut icon" href="./static/images/logo.ico" type="image/x-icon" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="description" content="吉林省女子职业教育指导中心" />
<meta name="keywords" content="吉林省女子职业教育指导中心" />
<title>吉林省女子职业教育指导中心</title>
<link href="./static/css/jlwj.css" type="text/css" rel="stylesheet" />

<link rel="stylesheet" type="text/css" href="<%=ser%>css/flexpaper.css" />
<script type="text/javascript" src="<%=ser%>js/jquery.min.js"></script>
<script type="text/javascript" src="<%=ser%>js/jquery.extensions.min.js"></script>
<script type="text/javascript" src="<%=ser%>js/flexpaper.js"></script>
<script type="text/javascript" src="<%=ser%>js/flexpaper_handlers.js"></script>

<!--[if IE 6]>
        <script type="text/javascript" src="./static/js/png.js"></script>
        <script type="text/javascript">
          DD_belatedPNG.fix('img');
        </script>
        <![endif]-->
<script type="text/javascript">
          $(function(){
            $(".nav ul li:has(ul)").hover(function(){
              $(this).children('ul').stop(true, true).slideDown(400);
            },function(){
              $(this).children('ul').stop(true, true).slideUp('fast')
            });
          }) 
        
         $(function(){
            $('.nav ul>li').hover(function(){
              $(this).addClass('active').siblings('.nav ul>li').removeClass('active');
            })
          })
        </script>
</head>

<body>
    <jsp:include page="/topmenu.jsp" />
  <div class="main">
    <div class="leftcont pull-right">
      <jsp:include page="contact_box.jsp" />
      <div class="clear"></div>
    </div>

    <div class="rightcont pull-left">
      <div class="website">
        您现在的位置：<a href="index.jsp">首页</a> > <a href="">培训教材 </a>
      </div>
      <div class="rightcont-cont">
        <h3>培训教材</h3>
        <!-- 此处添加教材显示区 -->
        <div style="text-align:center; margin-top: 20px; margin-bottom: 20px;">
          <h1>吉林网姐初级班培训教材</h1>
        </div>
        <div id="documentViewer" class="flexpaper_viewer" style="width:730px;height:700px"></div>
        <script type="text/javascript">   
          String.format = function() {
            var s = arguments[0];
            for (var i = 0; i < arguments.length - 1; i++) {
              var reg = new RegExp("\\{" + i + "\\}", "gm");
              s = s.replace(reg, arguments[i + 1]);
            }
            return s;
          };
          
          $('#documentViewer').FlexPaperViewer( {
            config : {
              SwfFile : "{static/doc/doc1.pdf_[*,0].swf,2}",    
              //SwfFile : "{static/doc/chuji.pdf_[*,0].swf,139}",  
              Scale : 0.6, 
              ZoomTransition : 'easeOut',
              ZoomTime : 0.5,
              ZoomInterval : 0.1,
              FitPageOnLoad : true,
              FitWidthOnLoad : true,
              FullScreenAsMaxWindow : false,
              ProgressiveLoading : false,
              MinZoomSize : 0.2,
              MaxZoomSize : 5,
              SearchMatchAll : false,           
              ViewModeToolsVisible : true,
              ZoomToolsVisible : true,
              NavToolsVisible : true,
              CursorToolsVisible : false,
              SearchToolsVisible : false,
              PrintToolsVisiable: false,
              jsDirectory : '<%=ser%>/js/',
              localeDirectory : '<%=ser%>/locale/',
              JSONDataType : 'jsonp',
              WMode : 'window',
              localeChain: 'zh_CN'
            }}
          );
        </script>
      </div>  
    </div>
    <div class="clear"></div>
    <div class="foot">
      版权所有：吉林省女子职业教育指导中心 Copyright © 2014 吉ICP备14003352号 <br />地址：长春市宽城区北亚泰大街1360号 主办单位：吉林省女子职业教育指导中心 电话：0431-81175117 / 81175118
    </div>
    <div align="center">
          <script type="text/javascript">document.write(unescape("%3Cspan id='_ideConac' %3E%3C/span%3E%3Cscript src='http://dcs.conac.cn/js/09/000/0000/60156293/CA090000000601562930003.js' type='text/javascript'%3E%3C/script%3E"));</script>
        </div>
  </div>
</body>
</html>
