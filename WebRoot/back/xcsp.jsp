﻿<%@ page language="java" pageEncoding="UTF-8"%>

<%
  String path = request.getContextPath();
  String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
    <link rel="shortcut icon" href="./static/images/logo.ico" type="image/x-icon" />
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="description" content="吉林省女子职业教育指导中心" />
        <meta name="keywords" content="吉林省女子职业教育指导中心" />
        <title>吉林省女子职业教育指导中心</title>
        <link href="./static/css/jlwj.css" type="text/css" rel="stylesheet"/> 
        <script  type="text/javascript" src="./static/js/jquery.min.js"></script>
        <!--[if IE 6]>
        <script type="text/javascript" src="./static/js/png.js"></script>
        <script type="text/javascript">
          DD_belatedPNG.fix('img');
        </script>
        <![endif]-->
        <script type="text/javascript">
          $(function(){
            $(".nav ul li:has(ul)").hover(function(){
              $(this).children('ul').stop(true, true).slideDown(400);
            },function(){
              $(this).children('ul').stop(true, true).slideUp('fast')
            });
          }) 
        
         $(function(){
            $('.nav ul>li').hover(function(){
              $(this).addClass('active').siblings('.nav ul>li').removeClass('active');
            })
          })
        </script>
    </head>
    
    <body>
    	<jsp:include page="/topmenu.jsp" />
		<div class="main">

         <div class="leftcont pull-left">
          <jsp:include page="zxgk_left.jsp" />
          <jsp:include page="contact_box.jsp" />
          <div class="clear"></div>
        </div> 

        <div class="rightcont pull-right">
           <div class="website">
            您现在的位置：<a href="index.jsp">首页</a>  > <a href="">走进中心 </a>
          </div>
      <div class="rightcont-cont">
        <h3>宣传视频</h3>
        <!--  
        <div class="rightcont-text">
          <embed src="http://player.youku.com/player.php/sid/XOTI5NzYzNjMy/v.swf?winType=adshow&VideoIDS=XOTI5NzYzNjMy&isAutoPlay=true" allowFullScreen="true" quality="high" width="730" height="600"
            align="middle" allowScriptAccess="always" type="application/x-shockwave-flash"></embed>
        </div>
        -->
        <div class="rightcont-text">
          <div id="a1"></div>
          <script type="text/javascript" src="/ckplayer/ckplayer.js" charset="utf-8"></script>
          <script type="text/javascript">
			var flashvars = {
				f : '<%=basePath %>video/hdvideo.flv',
				c : 0,
				p : 1
			};
			var params = {
				bgcolor : '#FFF',
				allowFullScreen : true,
				allowScriptAccess : 'always',
				wmode : 'transparent'
			};
			CKobject.embedSWF(
					'/ckplayer/ckplayer.swf',
					'a1', 
					'ckplayer_a1', 
					'730',
					'600', flashvars, params);
		  </script>
        </div>
      </div>
    </div>
        <div class="clear"></div>
        <div class="foot">
版权所有：吉林省女子职业教育指导中心 Copyright © 2014 吉ICP备14003352号 
<br/>地址：长春市宽城区北亚泰大街1360号   主办单位：吉林省女子职业教育指导中心   电话：0431-81175117 / 81175118 
        </div>
        <div align="center">
          <script type="text/javascript">document.write(unescape("%3Cspan id='_ideConac' %3E%3C/span%3E%3Cscript src='http://dcs.conac.cn/js/09/000/0000/60156293/CA090000000601562930003.js' type='text/javascript'%3E%3C/script%3E"));</script>
        </div>
      </div>   
    </body>
</html>
